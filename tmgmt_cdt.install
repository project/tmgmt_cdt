<?php

/**
 * @file
 * Install.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\tmgmt_cdt\TmgmtCdtHelper;

/**
 * Implements hook_install().
 */
function tmgmt_cdt_install() {

  \Drupal::state()->setMultiple(TmgmtCdtHelper::defaultOptions());

  // Add indexes.
  $spec = [
    'fields' => [],
  ];
  $spec['fields']['cdt_correlation_id'] = \Drupal::service('entity.definition_update_manager')->getFieldStorageDefinition('cdt_correlation_id', 'tmgmt_job')->getSchema()['columns']['value'];
  $spec['fields']['cdt_request_identifier'] = \Drupal::service('entity.definition_update_manager')->getFieldStorageDefinition('cdt_request_identifier', 'tmgmt_job')->getSchema()['columns']['value'];
  $serviceSchema = \Drupal::service('database')->schema();
  $serviceSchema->addIndex('tmgmt_job', 'tmgmt_job_field__cdt_correlation_id__index', ['cdt_correlation_id'], $spec);
  $serviceSchema->addIndex('tmgmt_job', 'tmgmt_job_field__cdt_request_identifier__index', ['cdt_request_identifier'], $spec);

  /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $storage */
  $storage = \Drupal::entityTypeManager()->getStorage('view');

  // Jobs.
  $tmgmt_job_overview = $storage->load('tmgmt_job_overview');
  if ($tmgmt_job_overview instanceof EntityInterface) {
    $tmgmt_job_overview_duplicate = $tmgmt_job_overview->createDuplicate();
    $tmgmt_job_overview_duplicate->set('id', 'tmgmt_job_overview_cdt_backup');
    $tmgmt_job_overview_duplicate->set('status', FALSE);
    $tmgmt_job_overview_duplicate->save();
    $tmgmt_job_overview->delete();
  }

  // Job items.
  $tmgmt_job_items = $storage->load('tmgmt_job_items');
  if ($tmgmt_job_items instanceof EntityInterface) {
    $tmgmt_job_items_duplicate = $tmgmt_job_items->createDuplicate();
    $tmgmt_job_items_duplicate->set('id', 'tmgmt_job_items_cdt_backup');
    $tmgmt_job_items_duplicate->set('status', FALSE);
    $tmgmt_job_items_duplicate->save();
    $tmgmt_job_items->delete();
  }

  // all_job_items.
  $tmgmt_translation_all_job_items = $storage->load('tmgmt_translation_all_job_items');
  if ($tmgmt_translation_all_job_items instanceof EntityInterface) {
    $tmgmt_translation_all_job_items_duplicate = $tmgmt_translation_all_job_items->createDuplicate();
    $tmgmt_translation_all_job_items_duplicate->set('id', 'tmgmt_translation_all_job_items_cdt_backup');
    $tmgmt_translation_all_job_items_duplicate->set('status', FALSE);
    $tmgmt_translation_all_job_items_duplicate->save();
    $tmgmt_translation_all_job_items->delete();
  }

  // Jobs.
  $path = \Drupal::service('extension.list.module')->getPath('tmgmt_cdt');
  $fileStorage = new FileStorage($path);

  $config = $fileStorage->read('config/views.view.tmgmt_job_overview');
  /** @var \Drupal\views\Entity\View $view */
  $view = $storage->create($config);
  if ($view) {
    $view->enable();
    $view->save();
  }

  // Job items.
  $config = $fileStorage->read('config/views.view.tmgmt_job_items');
  /** @var \Drupal\views\Entity\View $view */
  $view = $storage->create($config);
  if ($view) {
    $view->enable();
    $view->save();
  }

  // All job items.
  $config = $fileStorage->read('config/views.view.tmgmt_translation_all_job_items');
  /** @var \Drupal\views\Entity\View $view */

  $view = $storage->create($config);
  if ($view) {
    $view->enable();
    $view->save();
  }

}

/**
 * Implements hook_uninstall().
 */
function tmgmt_cdt_uninstall() {

  \Drupal::state()->deleteMultiple(array_keys(TmgmtCdtHelper::defaultOptions()));

  /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $storage */
  $storage = \Drupal::entityTypeManager()->getStorage('view');

  // Restore Views from TMGMT
  // Jobs.
  $tmgmt_job_overview = $storage->load('tmgmt_job_overview');
  if ($tmgmt_job_overview instanceof EntityInterface) {
    $tmgmt_job_overview->delete();
  }

  // Job items.
  $tmgmt_job_items = $storage->load('tmgmt_job_items');
  if ($tmgmt_job_items instanceof EntityInterface) {
    $tmgmt_job_items->delete();
  }

  // All job items.
  $tmgmt_translation_all_job_items = $storage->load('tmgmt_translation_all_job_items');
  if ($tmgmt_translation_all_job_items instanceof EntityInterface) {
    $tmgmt_translation_all_job_items->delete();
  }

  // Jobs.
  $tmgmt_job_overview_backup = $storage->load('tmgmt_job_overview_cdt_backup');
  if ($tmgmt_job_overview_backup instanceof EntityInterface) {
    $tmgmt_job_overview = $tmgmt_job_overview_backup->createDuplicate();
    $tmgmt_job_overview->set('id', 'tmgmt_job_overview');
    $tmgmt_job_overview->set('status', TRUE);
    $tmgmt_job_overview->save();
    $tmgmt_job_overview_backup->delete();
  }

  // Job items.
  $tmgmt_job_items_backup = $storage->load('tmgmt_job_items_cdt_backup');
  if ($tmgmt_job_items_backup instanceof EntityInterface) {
    $tmgmt_job_items = $tmgmt_job_items_backup->createDuplicate();
    $tmgmt_job_items->set('id', 'tmgmt_job_items');
    $tmgmt_job_items->set('status', TRUE);
    $tmgmt_job_items->save();
    $tmgmt_job_items_backup->delete();
  }

  // All job items.
  $tmgmt_translation_all_job_items_backup = $storage->load('tmgmt_translation_all_job_items_cdt_backup');
  if ($tmgmt_translation_all_job_items_backup instanceof EntityInterface) {
    $tmgmt_translation_all_job_items = $tmgmt_translation_all_job_items_backup->createDuplicate();
    $tmgmt_translation_all_job_items->set('id', 'tmgmt_translation_all_job_items');
    $tmgmt_translation_all_job_items->set('status', TRUE);
    $tmgmt_translation_all_job_items->save();
    $tmgmt_translation_all_job_items_backup->delete();
  }

  // Remove all tmgmt entities.
  $database = \Drupal::service('database');

  $query = $database->delete('tmgmt_job');
  $query->execute();

  $query = $database->delete('tmgmt_job_item');
  $query->execute();

  $query = $database->delete('tmgmt_message');
  $query->execute();

  $query = $database->delete('tmgmt_remote');
  $query->execute();

}
