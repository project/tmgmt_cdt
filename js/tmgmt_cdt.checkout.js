/**
 * @file
 */

(function ($) {
    Drupal.behaviors.checkout = {
        attach: function (context, settings) {
            $(".chosen-toggle").each(
                function (index) {
                    $(this).on(
                        "click", function () {
                            $("#edit-settings-cdt-target-languages")
                            .find("option")
                            .prop("selected", $(this).hasClass("select"))
                            .parent()
                            .trigger("chosen:updated");
                        }
                    );
                }
            );
        }
    };
})(jQuery);
