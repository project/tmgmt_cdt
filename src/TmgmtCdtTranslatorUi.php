<?php

namespace Drupal\tmgmt_cdt;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;

/**
 * CdtTmgmt translator UI.
 */
class TmgmtCdtTranslatorUi extends TranslatorPluginUiBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {

    $form = [];

    try {

      TmgmtCdtHelper::showStatus(TRUE);

      $job_check = tmgmt_cdt_check_job_status($job->id());
      if ($job_check !== FALSE) {
        $job = $job_check;
      }

      $cdt_request_identifier = trim((string) $job->get('cdt_request_identifier')->value);
      $cdt_correlation_id = trim((string) $job->get('cdt_correlation_id')->value);

      $details = $this->t('Reference (request identifier): <strong>@cdt_request_identifier</strong><br>', ['@cdt_request_identifier' => $cdt_request_identifier]);
      $details .= $this->t('Temporary id (correlation id): <strong>@cdt_correlation_id</strong><br>', ['@cdt_correlation_id' => $cdt_correlation_id]);

      $api = TmgmtCdtApi::getInstance();

      $api->setParamsPath(TmgmtCdtHelper::splitRequestIdentifier($cdt_request_identifier));
      $api->get('requests/{requestyear}/{requestnumber}');

      if (isset($api->response->content->status) && $api->response->status === 'success') {
        $details .= $this->t('CdT job status: <strong>@cdt_status</strong><br>', ['@cdt_status' => $api->response->content->status]);
      }
      else {
        $details .= $this->t('CdT job status: -<br>');
      }
      $form['job_information'] = [
        '#markup' => $details . '<br>',
      ];

      $cdt_settings = @Json::decode($job->get('cdt_settings')->value);
      $is_on_pickup = isset($cdt_settings['on_pickup']);

      if ($job->isActive()) {

        if ($api->response->status === 'success' && ($api->response->content->status === TmgmtCdtHelper::API_STATUS_COMPLETED)) {
          $this->messenger()->addStatus($this->t('Translations ready. Please click on the <strong>Pick up translations</strong> button 👇'));

          $form['group_pull'] = [
            '#type' => 'fieldset',
            '#description' => $is_on_pickup ? $this->t('Long pickup in progress, click again to retrieve more.') : '',
            '#weight' => 10,
            '#open' => TRUE,
          ];

          $form['group_pull']['actions']['pull'] = [
            '#type' => 'submit',
            "#button_type" => "primary",
            '#value' => $this->t('Pick up translations'),
            '#submit' => [[$this, 'submitPullTranslations']],
            '#weight' => -10,
          ];

        }
        elseif ($api->response->status === 'success' && ($api->response->content->status === TmgmtCdtHelper::API_STATUS_QUOTED_PENDING_APPROVAL)) {
          $this->messenger()->addStatus($this->t('Quotation ready. Please accept or reject the quotation 👇'));

          $api->resetInstance();
          $api->setParamsPath(TmgmtCdtHelper::splitRequestIdentifier($cdt_request_identifier));
          $api->get('requests/{requestyear}/{requestnumber}/quotationSummary');

          if (isset($api->response->content->totalPrice) && $api->response->status === 'success') {

            $form['group_quotation'] = [
              '#type' => 'fieldset',
              '#weight' => 10,
              '#open' => TRUE,
            ];
            $form['group_quotation']['details'] = [
              '#markup' => $this->t('Total price: <strong style="font-size: 150%;" > @price €</strong><br><br>', ['@price' => (int) $api->response->content->totalPrice]),
            ];

            $form['group_quotation']['actions']['accept_quotation'] = [
              '#type' => 'submit',
              "#button_type" => "primary",
              '#value' => $this->t('Accept quotation'),
              '#submit' => [[$this, 'submitAcceptQuotation']],
              '#weight' => -9,
            ];
            $form['group_quotation']['actions']['cancel_quotation'] = [
              '#type' => 'submit',
              "#button_type" => "primary",
              '#value' => $this->t('Cancel quotation'),
              '#submit' => [[$this, 'submitCancelQuotation']],
              '#weight' => -9,
            ];
            $form['group_quotation']['actions']['consent'] = [
              '#type' => 'checkbox',
              '#title' => $this->t('Yes I understand the implications of "accept quotation" and "cancel quotation" actions.'),
              '#default_value' => 0,
            ];
          }

        }
        else {
          $this->messenger()->addWarning($this->t('Translation/quotation in progress, you will be alerted when CdT delivers the requested products'));
        }

        $user = \Drupal::currentUser();

        if ($user->hasPermission('accept translation jobs') && $job->hasItemsToReview() && !$is_on_pickup) {
          $form['group_accept'] = [
            '#type' => 'fieldset',
            '#weight' => 10,
            '#open' => TRUE,
          ];

          $form['group_accept']['actions']['accept_all_translation'] = [
            '#type' => 'submit',
            "#button_type" => "primary",
            '#value' => $this->t('Accept all translations'),
            '#submit' => [[$this, 'submitAcceptAllTranslations']],
            '#weight' => -9,
          ];
          $form['group_accept']['actions']['consent'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Yes I understand that the translations will be automatically published without review <strong>(Not recommended)</strong>.'),
            '#default_value' => 0,
          ];

        }

      }

      return $form;

    }
    catch (\Exception $e) {

      $this->messenger()->addError($e->getMessage());

      return parent::checkoutInfo($job);
    }

  }

  /**
   * Submit button pull translations.
   */
  public function submitPullTranslations(array $form, FormStateInterface $form_state) {
    $this->messenger()->deleteAll();
    /**
     * @var \Drupal\tmgmt\TranslatorInterface $translator
     */
    $job = $form_state->getFormObject()->getEntity();

    tmgmt_cdt_pull_translations($job);

  }

  /**
   * Submit button accept all translations.
   */
  public function submitAcceptAllTranslations(array $form, FormStateInterface $form_state) {

    $this->messenger()->deleteAll();

    if (!(bool) $form_state->getValues()['consent']) {
      $this->messenger()->addError($this->t('You forgot the checkbox "Yes I understand ..."'));
      return;
    }

    $job = $form_state->getFormObject()->getEntity();
    $job->acceptTranslation();

    if (tmgmt_job_check_finished($job->id())) {
      $job->setState(JobInterface::STATE_FINISHED);
      $job->set('target_language', 'multiple');
      $job->save();
      $job->addMessage($this->t('The translation job has been finished. (user action "Accept all translations"'));
    }

  }

  /**
   * Submit button accept quotation.
   */
  public function submitAcceptQuotation(array $form, FormStateInterface $form_state) {
    $this->messenger()->deleteAll();
    /**
     * @var \Drupal\tmgmt\TranslatorInterface $translator
     */
    $job = $form_state->getFormObject()->getEntity();

    if (!(bool) $form_state->getValues()['consent']) {
      $this->messenger()->addError($this->t('You forgot the checkbox "Yes I understand ..."'));
      return;
    }

    $cdt_request_identifier = trim((string) $job->get('cdt_request_identifier')->value);

    $api = TmgmtCdtApi::getInstance();
    $api->setParamsPath(TmgmtCdtHelper::splitRequestIdentifier($cdt_request_identifier));
    $api->post('requests/{requestyear}/{requestnumber}/ApproveQuotation');

    if ($api->response->status === 'success') {
      $job->addMessage($this->t('The quotation has been accepted.'));
      $this->messenger()->addStatus($this->t('The quotation has been accepted.'));
    }
    else {
      $this->messenger()->addError($this->t('Error on processing the action'));
      $this->messenger()->addError(
            $this->t(
                '@error',
                ['@error' => implode(', ', $api->response->errors)]
            )
            );
    }

  }

  /**
   * Submit button accept quotation.
   */
  public function submitCancelQuotation(array $form, FormStateInterface $form_state) {
    $this->messenger()->deleteAll();
    /**
     * @var \Drupal\tmgmt\TranslatorInterface $translator
     */
    $job = $form_state->getFormObject()->getEntity();

    if (!(bool) $form_state->getValues()['consent']) {
      $this->messenger()->addError($this->t('You forgot the checkbox "Yes I understand ..."'));
      return;
    }

    $cdt_request_identifier = trim((string) $job->get('cdt_request_identifier')->value);

    $api = TmgmtCdtApi::getInstance();
    $api->setParamsPath(TmgmtCdtHelper::splitRequestIdentifier($cdt_request_identifier));
    $api->post('requests/{requestyear}/{requestnumber}/CancelQuotation');

    if ($api->response->status === 'success') {
      $job->addMessage($this->t('The quotation has been cancelled.'));
      $this->messenger()->addStatus($this->t('The quotation has been cancelled.'));
    }
    else {
      $this->messenger()->addError($this->t('Error on processing the action'));
      $this->messenger()->addError(
            $this->t(
                '@error',
                ['@error' => implode(', ', $api->response->errors)]
            )
            );
    }

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    TmgmtCdtHelper::showStatus();

    $this->messenger()->deleteAll();

    $form['conf_mode'] = [
      '#type' => 'details',
      '#title' => 'Environment selection',
      '#weight' => 10,
      '#open' => TRUE,

    ];
    $form['conf_mode']['api_mode'] = [
      '#type' => 'select',
      '#default_value' => TmgmtCdtHelper::getState('api_mode'),
      '#options' => [
        TmgmtCdtHelper::SIMULATION => $this->t('Simulation (Not connected)'),
        TmgmtCdtHelper::PRODUCTION => $this->t('Real (connected)'),
      ],

    ];

    $form['conf_credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('Web service connection'),
      '#weight' => 20,

      '#open' => FALSE,
    ];

    $credentials = TmgmtCdtHelper::getCredentials();
    if ($credentials['source'] === 'settings') {
      $form['conf_credentials']['#description'] = $this->t('Credentials not editable on this instance.');
      $form['conf_credentials']['#description'] .= '<br>Url: <strong>' . $credentials['api_url'] . '</strong>';
      $form['conf_credentials']['#description'] .= '<br>Username: <strong>' . $credentials['api_username'] . '</strong>';
      $form['conf_credentials']['#description'] .= '<br>Password: <strong>' . str_repeat("*", strlen(\base64_decode($credentials['api_username']))) . '</strong>';

    }
    else {
      $form['conf_credentials']['api_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Web service URL'),
        '#default_value' => $credentials['api_url'],
        '#size' => 100,
        '#maxlength' => 255,
        '#description' => $this->t('Example: <i>https://b2becdt-a.cdt.europa.eu/generic-rest/</i>'),
      ];
      $form['conf_credentials']['api_username'] = [
        '#type' => 'textfield',
        '#title' => 'API username',
        '#default_value' => $credentials['api_username'],
        '#size' => 30,
        '#maxlength' => 50,
      ];
      $form['conf_credentials']['api_password'] = [
        '#type' => 'password',
        '#title' => $this->t('API password'),
        '#size' => 30,
        '#maxlength' => 50,
      ];
    }

    $form['conf_advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
      '#description' => '<strong>' . $this->t('Please read the documentation carefully before changing these options') . '</strong><br><br>',
      '#weight' => 30,
      '#open' => FALSE,
    ];
    $form['conf_advanced']['use_token_html_a'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable tags <u><i>&lt;a&gt;</i></u> conversion.'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('use_token_html_a'),
    ];
    $form['conf_advanced']['use_token_html_img'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable tags <u><i>&lt;img&gt;</i></u> conversion.'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('use_token_html_img'),
    ];
    $form['conf_advanced']['use_token_html_script'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable tags <u><i>&lt;script&gt;</i></u> conversion. <strong>(Strongly recommended)</strong>'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('use_token_html_script'),
    ];
    $form['conf_advanced']['use_token_drupal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Drupal tags like: <u><i>&lt;front&gt;, [[{&quot;fid&quot;: ..]], [token]</i></u> conversion. <strong>(Strongly recommended)</strong>'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('use_token_drupal'),
    ];
    $form['conf_advanced']['html_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable html verification. <strong>(Strongly recommended)</strong>'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('html_check'),
    ];
    $form['conf_advanced']['max_pages_per_request'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum number of pages per request'),
      '#default_value' => (int) TmgmtCdtHelper::getState('max_pages_per_request'),
      '#size' => 5,
    ];
    $form['conf_advanced']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log debugging information for API calls. 🚧'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('debug'),
    ];
    $form['conf_advanced']['xsd_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('XSD Schema link.'),
      '#default_value' => TmgmtCdtHelper::getState('xsd_link'),
      '#size' => 100,
      '#maxlength' => 255,
    ];
    $form['conf_advanced']['disable_xsd_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable XSD schema validation. <strong>(Not recommended)</strong>'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('disable_xsd_validation'),
    ];
    $form['conf_advanced']['exclude_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Fields name to exclude. (Semicolon separated values, no space between elements)'),
      '#description' => $this->t('Use resName value in XML file'),
      '#default_value' => TmgmtCdtHelper::getState('exclude_fields'),
      '#cols' => 50,
      '#rows' => 3,
    ];
    $form['conf_advanced']['disable_simulator_injection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable simulator html injection (language code)'),
      '#default_value' => (bool) TmgmtCdtHelper::getState('disable_simulator_injection'),
    ];
    $form['conf_advanced']['job_max_items'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum number of items for a job'),
      '#default_value' => (int) TmgmtCdtHelper::getState('job_max_items'),
      '#size' => 5,
    ];
    $form['conf_advanced']['job_max_process'] = [
      '#type' => 'textfield',
      '#title' => $this->t('maximum number of items to process in a job'),
      '#description' => $this->t('For "pickup translations" and "accept all translations" buttons (max per click)'),
      '#default_value' => (int) TmgmtCdtHelper::getState('job_max_process'),
      '#size' => 5,
    ];

    $form['conf_network'] = [
      '#type' => 'details',
      '#title' => $this->t('Network'),
      '#weight' => 40,
      '#open' => FALSE,
    ];
    $form['conf_network']['curl_proxy_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy host'),
      '#default_value' => TmgmtCdtHelper::getState('curl_proxy_host'),
      '#size' => 50,
      '#maxlength' => 100,
    ];
    $form['conf_network']['curl_proxy_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy port'),
      '#default_value' => (int) TmgmtCdtHelper::getState('curl_proxy_port'),
      '#size' => 5,
      '#maxlength' => 10,
    ];
    $form['conf_network']['curl_proxy_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy username'),
      '#default_value' => TmgmtCdtHelper::getState('curl_proxy_user'),
      '#size' => 30,
      '#maxlength' => 50,
    ];
    $form['conf_network']['curl_proxy_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Proxy password'),
      '#default_value' => TmgmtCdtHelper::getState('curl_proxy_pass'),
      '#size' => 30,
      '#maxlength' => 50,
    ];
    $form['conf_network']['curl_proxy_user_agent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy user agent'),
      '#default_value' => TmgmtCdtHelper::getState('curl_proxy_user_agent'),
      '#size' => 100,
      '#maxlength' => 255,
    ];
    $form['conf_network']['curl_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CURL options (JSON format).'),
      '#default_value' => TmgmtCdtHelper::getState('curl_options'),
      '#cols' => 50,
      '#rows' => 5,
    ];

    $form['conf_operations'] = [
      '#type' => 'details',
      '#title' => $this->t('Operations'),
      '#weight' => 50,
      '#open' => FALSE,
    ];
    $form['conf_operations']['operation'] = [
      '#type' => 'select',
      '#options' => [
        'select' => $this->t('Select operation ...'),
        'reset' => $this->t('Reset all values <strong>(All your custom options will be deleted)</strong>'),
        'log_proxy_settings' => $this->t('Export proxy settings to Drupal logger.'),
      ],
    ];
    $form['conf_operations']['validate_operation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Yes I understand what I do.'),
      '#default_value' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

    $conf_operations = $form_state->getValues()['settings']['conf_operations'];
    $form_state->unsetValue(['settings', 'conf_credentials']);
    $form_state->unsetValue(['settings', 'conf_operations']);

    parent::validateConfigurationForm($form, $form_state);

    /**
     * @var \Drupal\tmgmt\TranslatorInterface $translator
     */
    $translator = $form_state->getFormObject()->getEntity();

    $this->messenger()->deleteAll();

    if (($conf_operations['operation'] !== 'select') && !((bool) $conf_operations['validate_operation'])) {
      $this->messenger()->addWarning($this->t('You forgot to check "Yes I understand ...'));
      return;
    }
    if ((bool) $conf_operations['validate_operation']) {
      if ($conf_operations['operation'] === 'reset') {
        \Drupal::state()->setMultiple(TmgmtCdtHelper::defaultOptions());
        $this->messenger()->addWarning($this->t('All options have been reset.'));
        return;
      }
      if ($conf_operations['operation'] === 'log_proxy_settings') {
        \Drupal::logger('tmgmt_cdt')->debug('http_client_config: <pre><code>' . print_r(Settings::get('http_client_config'), TRUE) . '</code></pre>');
        $this->messenger()->addWarning($this->t('http_client_config settings exported to Drupal logger.'));
        return;
      }

    }

    $is_ready = TRUE;

    try {

      // Reset.
      \Drupal::state()->set('tmgmt_cdt.ready', FALSE);
      \Drupal::state()->set('tmgmt_cdt.business_reference_data', new \stdClass());
      \Drupal::state()->set('tmgmt_cdt.api_access_token_expire', 0);
      \Drupal::state()->set('tmgmt_cdt.api_access_token', '');

      // Mode.
      \Drupal::state()->set('tmgmt_cdt.api_mode', $translator->getSetting('conf_mode')['api_mode']);

      // Webservice.
      $credentials = TmgmtCdtHelper::getCredentials();
      if ($credentials['source'] !== 'settings') {
        \Drupal::state()->set('tmgmt_cdt.api_url', @trim((string) $translator->getSetting('conf_credentials')['api_url']));
        \Drupal::state()->set('tmgmt_cdt.api_username', @trim((string) $translator->getSetting('conf_credentials')['api_username']));
        if (mb_strlen(@trim((string) $translator->getSetting('conf_credentials')['api_password'])) > 0) {
          \Drupal::state()->set('tmgmt_cdt.api_password', base64_encode(@trim((string) $translator->getSetting('conf_credentials')['api_password'])));
        }
      }

      // Advanced.
      \Drupal::state()->set('tmgmt_cdt.use_token_html_a', (bool) $translator->getSetting('conf_advanced')['use_token_html_a']);
      \Drupal::state()->set('tmgmt_cdt.use_token_html_img', (bool) $translator->getSetting('conf_advanced')['use_token_html_img']);
      \Drupal::state()->set('tmgmt_cdt.use_token_html_script', (bool) $translator->getSetting('conf_advanced')['use_token_html_script']);
      \Drupal::state()->set('tmgmt_cdt.use_token_drupal', (bool) $translator->getSetting('conf_advanced')['use_token_drupal']);
      \Drupal::state()->set('tmgmt_cdt.html_check', (bool) $translator->getSetting('conf_advanced')['html_check']);
      \Drupal::state()->set('tmgmt_cdt.max_pages_per_request', (int) $translator->getSetting('conf_advanced')['max_pages_per_request']);
      \Drupal::state()->set('tmgmt_cdt.debug', (bool) $translator->getSetting('conf_advanced')['debug']);
      \Drupal::state()->set('tmgmt_cdt.xsd_link', $translator->getSetting('conf_advanced')['xsd_link']);

      if (TmgmtCdtHelper::getState('api_mode') === TmgmtCdtHelper::SIMULATION) {
        $relative_path = \Drupal::service('module_handler')->getModule('tmgmt_cdt')->getPath() . '/xml/schema.xsd';
        $full_path = \Drupal::service('file_system')->realpath($relative_path);
        $xml_contents = \file_get_contents($full_path);
        \Drupal::state()->set('tmgmt_cdt.xsd_source', $xml_contents);

      }
      else {
        if (!empty(trim((string) $translator->getSetting('conf_advanced')['xsd_link']))) {
          $schemaSource = TmgmtCdtHelper::curlGetFileContents(trim((string) $translator->getSetting('conf_advanced')['xsd_link']));
          if ($schemaSource !== FALSE) {
            \Drupal::state()->set('tmgmt_cdt.xsd_source', $schemaSource);
          }
          else {
            \Drupal::state()->set('tmgmt_cdt.xsd_source', '');
          }
        }
        else {
          \Drupal::state()->set('tmgmt_cdt.xsd_source', '');
        }

      }

      \Drupal::state()->set('tmgmt_cdt.disable_xsd_validation', (bool) $translator->getSetting('conf_advanced')['disable_xsd_validation']);
      \Drupal::state()->set('tmgmt_cdt.exclude_fields', trim((string) $translator->getSetting('conf_advanced')['exclude_fields']));
      \Drupal::state()->set('tmgmt_cdt.disable_simulator_injection', (bool) $translator->getSetting('conf_advanced')['disable_simulator_injection']);
      \Drupal::state()->set('tmgmt_cdt.job_max_items', (int) $translator->getSetting('conf_advanced')['job_max_items']);
      \Drupal::state()->set('tmgmt_cdt.job_max_process', (int) $translator->getSetting('conf_advanced')['job_max_process']);

      // Network.
      \Drupal::state()->set('tmgmt_cdt.curl_proxy_host', $translator->getSetting('conf_network')['curl_proxy_host']);
      \Drupal::state()->set('tmgmt_cdt.curl_proxy_port', (int) $translator->getSetting('conf_network')['curl_proxy_port']);
      \Drupal::state()->set('tmgmt_cdt.curl_proxy_user', $translator->getSetting('conf_network')['curl_proxy_user']);
      if (mb_strlen(trim((string) $translator->getSetting('conf_network')['curl_proxy_pass'])) > 0) {
        \Drupal::state()->set('tmgmt_cdt.curl_proxy_pass', $translator->getSetting('conf_network')['curl_proxy_pass']);
      }
      \Drupal::state()->set('tmgmt_cdt.curl_proxy_user_agent', $translator->getSetting('conf_network')['curl_proxy_user_agent']);
      if (TmgmtCdtHelper::isJson(trim((string) $translator->getSetting('conf_network')['curl_options']))) {
        \Drupal::state()->set('tmgmt_cdt.curl_options', trim((string) $translator->getSetting('conf_network')['curl_options']));
      }
      else {
        $is_ready = FALSE;
        throw new \Exception('Wrong JSON format for Curl options');
      }

      // Check part.
      $is_ready = TRUE;
      $this->messenger()->addStatus($this->t('Check <strong>@name</strong> translator settings:', ['@name' => TmgmtCdtHelper::NAME]));
      $this->messenger()->addStatus($this->t('Current environment: <strong>@mode</strong>.', ['@mode' => TmgmtCdtHelper::getState('api_mode')]));

      $api = TmgmtCdtApi::getInstance();
      $api->setReady(TRUE);

      // Check connectivity to webservice.
      if ($api->checkConnection() === TRUE) {
        $this->messenger()->addStatus($this->t('Connection to CDT webservice: <strong>OK</strong>.'));
      }
      else {
        $this->messenger()->addError(
              $this->t(
                  'Connection to CDT webservice: <strong>FAILED</strong>. @error',
                  ['@error' => implode(', ', $api->response->errors)]
              )
                );
        return;
      }

      // Check languages list.
      if (\count((array) \Drupal::languageManager()->getLanguages()) > 1) {
        $this->messenger()->addStatus($this->t('Language list: <strong>OK</strong>.'));
      }
      else {
        $is_ready = FALSE;
        $this->messenger()->addError($this->t('Language list: <strong>FAILED</strong>. @error', ['@error' => 'There must be at least 2 active languages.']));
      }

      // Get business reference data.
      $api->resetInstance();
      // $api->setMode(TmgmtCdtHelper::PRODUCTION);
      $api->setReady(TRUE);
      $api->get('requests/businessReferenceData');

      if ($api->response->status === 'success') {
        \Drupal::state()->set('tmgmt_cdt.business_reference_data', $api->response->content);
        $this->messenger()->addStatus($this->t('Business reference data: <strong>OK</strong>.'));
      }
      else {
        $is_ready = FALSE;
        $this->messenger()->addError(
              $this->t(
                  'Business reference data: <strong>FAILED</strong>. @error',
                  ['@error' => implode(', ', $api->response->errors)]
              ),
              'error', FALSE
                );
      }

      $json_file = \Drupal::service('file_system')->realpath(\Drupal::service('module_handler')->getModule('tmgmt_cdt')->getPath() . '/json/fakeRequest.json');
      $fake_request = @\file_get_contents($json_file);
      if (!$fake_request) {
        $is_ready = FALSE;
        $this->messenger()->addError(
              $this->t(
                  'Cannot read the file: tmgmt_cdt/json/fakeRequest.json'
              ),
              'error', FALSE
          );
      }

      $fake_request = @\json_decode($fake_request);
      if (!$fake_request) {
        $is_ready = FALSE;
        $this->messenger()->addError(
              $this->t(
                  'Error decoding json: tmgmt_cdt/json/fakeRequest.json'
              ),
              'error', FALSE
          );
      }

      $business_reference_data = TmgmtCdtHelper::getState('business_reference_data');
      $fake_request->departmentCode = $business_reference_data->departments[0]->code;
      $fake_request->contactUserNames = [$business_reference_data->contacts[0]->userName];
      $fake_request->deliveryContactUsernames = [$business_reference_data->contacts[0]->userName];

      $api->resetInstance();
      $api->setReady(TRUE);
      $api = TmgmtCdtApi::getInstance();

      if ($api->validateRequest($fake_request)) {
        $this->messenger()->addStatus($this->t('CdT web service user has permission to send a job: <strong>OK</strong>.'));
      }
      else {
        $is_ready = FALSE;
        $this->messenger()->addError(
              $this->t(
                  'CdT web service user has permission to send a job: <strong>FAILED</strong>. @error',
                  ['@error' => implode(', ', $api->response->errors)]
              ),
              'error', FALSE
                );
      }

      // Check missing languages mapping.
      $error_missing_languages = [];
      $api_languages = @(array) TmgmtCdtHelper::getState('business_reference_data')->languages;

      $languages_map = [];
      foreach (\Drupal::languageManager()->getLanguages() as $lang) {
        $remote_lang = $translator->mapToRemoteLanguage($lang->getId());

        if (!in_array($remote_lang, $api_languages)) {
          $error_missing_languages[] = $lang->getId();
        }
        $languages_map[$lang->getId()] = $remote_lang;
      }

      if (\count((array) $error_missing_languages) > 0) {
        $is_ready = FALSE;
        $this->messenger()->addError(
              $this->t(
                  'Languages mapping list: <strong>FAILED</strong>. These language codes must be mapped to the web service: @langs.', [
                    '@langs' => implode(', ', $error_missing_languages),
                  ]
              )
          );
      }
      else {
        $this->messenger()->addStatus($this->t('Languages mapping list: <strong>OK</strong>.'));
      }

      // Check duplicate languages mapping.
      $temp_list = [];
      $warning_duplicate_languages = [];
      foreach ($languages_map as $lang_local => $lang_remote) {
        if (!in_array($lang_remote, $temp_list)) {
          $temp_list[] = $lang_remote;
        }
        else {
          $warning_duplicate_languages[$lang_remote] = $lang_local;
        }
      }

      if (\count(array_keys($warning_duplicate_languages)) > 0) {
        $this->messenger()->addWarning(
            $this->t(
                'Languages duplicate mapping: <strong>WARNING</strong>. These language codes (CDT) are mapped twice or more: @langs.', [
                  '@langs' => implode(', ', array_keys($warning_duplicate_languages)),
                ]
            )
        );
      }
      else {
        $this->messenger()->addStatus($this->t('Languages duplicate mapping: <strong>OK</strong>.'));
      }

      // Check translators list.
      $query = \Drupal::entityQuery('tmgmt_translator')
        ->accessCheck(FALSE)
        ->condition('plugin', 'tmgmt_cdt', '<>');
      $result = $query->execute();

      if (!empty($result)) {
        $is_ready = FALSE;
        $this->messenger()->addError($this->t('Translators list: <strong>FAILED</strong>. Please delete the other translators.'), FALSE);
      }
      else {
        $this->messenger()->addStatus($this->t('Translators list: <strong>OK</strong>.'));
      }

      if (!$is_ready) {
        throw new \Exception('Errors detected. please correct your configuration according to the messages.');
      }

      \Drupal::state()->set('tmgmt_cdt.ready', TRUE);
    }
    catch (\Exception $e) {
      \Drupal::state()->set('tmgmt_cdt.ready', FALSE);
      $this->messenger()->addError($e->getMessage());
    }
    TmgmtCdtHelper::showStatus(TRUE);

  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {

    $job_settings = [];
    $errors = 0;

    $user_cdt_default = @Json::decode(\Drupal::service('user.data')->get('tmgmt_cdt', \Drupal::currentUser()->id(), 'cdt_default'));

    if (!$user_cdt_default) {
      $user_cdt_default = [];
    }

    // Load job settings if existe, if not load default user settings.
    if ($job->getSetting('cdt')) {
      $job_settings['cdt'] = $job->getSetting('cdt');
    }
    else {
      if (\count((array) $user_cdt_default) > 0) {
        $job_settings['cdt'] = $user_cdt_default;
        unset($job_settings['cdt']['related_jobs']);
      }
    }

    $business_reference_data = TmgmtCdtHelper::getState('business_reference_data');
    if (empty((array) $business_reference_data)) {
      $this->messenger()->addError($this->t('Cannot get Business Reference Data from <strong>@name</strong>', ['@name' => TmgmtCdtHelper::NAME]));
      $errors++;
    }

    $form['cdt']['request_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Service'),
      '#options' => [
        'TRANSLATION' => $this->t('Translation'),
        'LIGHT_POST_EDITING' => $this->t('Light post-editing'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('<strong>Translation service</strong>: <i>High-quality translation produced by a professional translator.</i><p>
      <strong>Light post-editing service</strong>: <i>This service includes editing, modifying and/or correcting machine translation. The final text is mainly to communicate the essential meaning without necessarily being perfect in the target language.</i>'),
    ];

    $form['cdt']['quotation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Quotation request only'),
      '#default_value' => $job_settings['cdt']['quotation'] ?? NULL,
      '#description' => $this->t('If you would like the Translation Centre to provide the volumes, prices and delivery deadlines for a request before deciding whether to submit it, you can ask for a quotation. Please note that the processing of Quotation requests requires an additional processing time depending on the volume and type of content. In case of doubt, please contact the CdT.'),
    ];

    $form['cdt']['target_languages'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Target languages'),
      '#chosen' => TRUE,
      '#options' => tmgmt_available_languages([\Drupal::languageManager()->getDefaultLanguage()->getId()]),
      '#default_value' => $job_settings['cdt']['target_languages'] ?? NULL,
      '#required' => TRUE,
      '#description' => '<a class="button chosen-toggle select">' . $this->t('Select all') . '</a> <a class="button chosen-toggle deselect">' . $this->t('Deselect all') . '</a>' . $this->t('Select target language(s) for the request'),
    ];
    $form['#attached']['library'][] = 'tmgmt_cdt/checkout';
    $priority_list = [];
    if (!isset($business_reference_data->priorities)) {
      $this->messenger()->addError($this->t('Cannot find the priorities list'));
      $errors++;
    }
    foreach ($business_reference_data->priorities as $priority) {
      $priority_list[$priority->code] = $priority->description;
    }
    $form['cdt']['priority'] = [
      '#type' => 'select',
      '#title' => $this->t('Priority'),
      '#options' => $priority_list,
      '#default_value' => $job_settings['cdt']['priority'] ?? NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select priority for the request'),
    ];
    unset($priority_list);

    $confidentiality_list = [];
    if (!isset($business_reference_data->confidentialities)) {
      $this->messenger()->addError($this->t('Cannot find the confidentialities list'));
      $errors++;
    }
    foreach ($business_reference_data->confidentialities as $confidentiality) {
      $confidentiality_list[$confidentiality->code] = $confidentiality->description;
    }
    $form['cdt']['confidentiality'] = [
      '#type' => 'select',
      '#title' => $this->t('Confidentiality'),
      '#options' => $confidentiality_list,
      '#default_value' => $job_settings['cdt']['confidentiality'] ?? NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select confidentiality for the request'),
    ];
    unset($confidentiality_list);

    $department_list = [];
    if (!isset($business_reference_data->departments)) {
      $errors++;
    }
    foreach ($business_reference_data->departments as $department) {
      $department_list[$department->code] = $department->description;
    }
    $form['cdt']['department'] = [
      '#type' => 'select',
      '#title' => $this->t('Department'),
      '#options' => $department_list,
      '#default_value' => $job_settings['cdt']['department'] ?? NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select department for the request'),
    ];
    unset($department_list);

    $contact_list = [];
    if (!isset($business_reference_data->contacts)) {
      $this->messenger()->addError($this->t('Cannot find the contacts list'));
      $errors++;
    }
    foreach ($business_reference_data->contacts as $contact) {
      $contact_list[$contact->userName] = $contact->firstName . ' ' . $contact->lastName;
    }
    asort($contact_list);

    $form['cdt']['contacts'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Contacts'),
      '#chosen' => TRUE,
      '#options' => $contact_list,
      '#default_value' => isset($job_settings['cdt']['contacts']) ? array_values($job_settings['cdt']['contacts']) : NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select contact(s) for the request'),
    ];

    $form['cdt']['deliver_to'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Deliver to'),
      '#chosen' => TRUE,
      '#options' => $contact_list,
      '#default_value' => isset($job_settings['cdt']['deliver_to']) ? array_values($job_settings['cdt']['deliver_to']) : NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select recipient contact(s) for the request'),
    ];
    unset($contact_list);

    $form['cdt']['options'] = [
      '#type' => 'select',
      '#title' => $this->t('Translation options'),
      '#options' => TmgmtCdtHelper::API_COMMENT_URLS,
      '#default_value' => $job_settings['cdt']['options'] ?? NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select the option according to your needs'),
    ];

    $query = \Drupal::entityQuery('tmgmt_job')
      ->accessCheck(FALSE)
      ->condition('translator', 'tmgmt_cdt')
      ->condition(
              'state', [JobInterface::STATE_ACTIVE,
                JobInterface::STATE_FINISHED,
              ], 'IN'
          );
    $ids = $query->execute();

    $ref_jobs = \Drupal::entityTypeManager()->getStorage('tmgmt_job')->loadMultiple($ids);

    $related_jobs_list = [];
    foreach ($ref_jobs as $refJob) {
      $related_jobs_list[$refJob->get('cdt_request_identifier')->value] = $refJob->get('label')->value;
    }

    $form['cdt']['related_jobs'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Related jobs'),
      '#options' => $related_jobs_list,
      '#chosen' => TRUE,
      '#default_value' => \count((array) @$job_settings['cdt']['related_jobs']) > 1 ? array_values($job_settings['cdt']['related_jobs']) : -1,
      '#description' => $this->t('Select jobs linked to this one'),
    ];
    unset($related_jobs_list);

    $form['cdt']['phone'] = [
      '#type' => 'number',
      '#title' => $this->t('Phone'),
      '#default_value' => $job_settings['cdt']['phone'] ?? '+0',
      '#size' => 20,
      '#maxlength' => 15,
      '#description' => $this->t('15 numeric characters maximum'),
      '#pattern' => '[^\\d]*',
    ];

    $form['cdt']['comments'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comments'),
      '#default_value' => $job_settings['cdt']['comments'] ?? '',
      '#cols' => 30,
      '#rows' => 3,
    ];

    $translator = $job->getTranslator();
    $remote_pair = [];

    if (@$form_state->getUserInput()['settings']['cdt']['target_languages']) {
      foreach ($form_state->getUserInput()['settings']['cdt']['target_languages'] as $target_language) {
        $remote_language = $translator->mapToRemoteLanguage($target_language);
        $remote_pair[$remote_language][] = $target_language;
      }
    }

    $remote_pair = array_filter($remote_pair, function ($pair) {
      return \count($pair) > 1;
    });

    if (\count($remote_pair) > 0) {

      $inverted_list = [];
      foreach ($remote_pair as $k => $v) {
        foreach ($v as $l) {
          $inverted_list[] = "$l > $k";
        }
      }

      $this->messenger()->addError($this->t(
          'You cannot create a job with multiple Drupal languages to a single CdT language.<br> Please change your target languages or update your languages mapping in CdT settings.<br> (@pairs)',
          ['@pairs' => implode(', ', $inverted_list)]
      ));
      $form_state->setErrorByName('cdt', '');
    }

    if ($errors === 0) {
      return parent::checkoutSettingsForm($form, $form_state, $job);
    }
    else {
      $this->messenger()->addError($this->t('Errors detected. please check the CdT configuration'));
    }

  }

}
