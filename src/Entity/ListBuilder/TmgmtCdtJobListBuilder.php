<?php

namespace Drupal\tmgmt_cdt\Entity\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\Entity\ListBuilder\JobListBuilder;

/**
 * Provides the views data for the message entity type.
 */
class TmgmtCdtJobListBuilder extends JobListBuilder {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if ($entity->isSubmittable() && $entity->access('submit')) {
      $operations['submit'] = [
        'url' => $entity->toUrl(),
        'title' => $this->t('Submit'),
        'weight' => -10,
      ];
    }
    else {
      $operations['manage'] = [
        'url' => $entity->toUrl(),
        'title' => $this->t('Manage'),
        'weight' => -10,
      ];
    }
    return $operations;
  }

}
