<?php

namespace Drupal\tmgmt_cdt\Entity\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\Entity\ListBuilder\JobItemListBuilder;

/**
 * Provides the views data for the message entity type.
 */
class TmgmtCdtJobItemListBuilder extends JobItemListBuilder {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if ($entity->getCountTranslated() > 0 && $entity->access('accept')) {
      $operations['review'] = [
        'url' => $entity->toUrl(),
        'title' => $this->t('Review'),
      ];
    }
    // Do not display view on unprocessed jobs.
    elseif (!$entity->getJob()->isUnprocessed()) {
      $operations['view'] = [
        'url' => $entity->toUrl(),
        'title' => $this->t('View'),
      ];
    }
    return $operations;
  }

}
