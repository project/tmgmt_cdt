<?php

namespace Drupal\tmgmt_cdt\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt_cdt\TmgmtCdtFormatHelper;

/**
 * Override classe JobItem.
 */
class TmgmtCdtJobItem extends JobItem {

  use StringTranslationTrait;

  /**
   * For job items in cart without job attached.
   *
   * @var bool
   */
  public $onCart = FALSE;

  /**
   * {@inheritdoc}
   */
  public function isAbortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation() {
    throw new TMGMTException('Function disabled. Please check the CdT documentation');
  }

  /**
   * {@inheritdoc}
   */
  public function acceptTranslation() {
    try {
      // To not break the tmgmt logic (source and target language at job level)
      $job = $this->getJob();
      $job->set('target_language', $this->get('cdt_target_language')->value);
      $job->save();

      $parent = parent::acceptTranslation();

      if (tmgmt_job_check_finished($job->id())) {
        $job->finished();
      }

      // And back to multiple.
      $job->set('target_language', 'multiple');
      $job->save();

      return $parent;

    }
    catch (\Exception $e) {

      \Drupal::messenger()->deleteAll();
      \Drupal::messenger()->addError(
            $this->t(
                'Error accepting translation of Job jobItem @tjid: @error',
                ['@tjid' => $this->id(), '@error' => $e->getMessage()]
            )
        );

    }

  }

  /**
   * {@inheritdoc}
   */
  protected function count(&$item) {
    @parent::count($item);
    if (!empty($item['#text'])) {
      if (\Drupal::service('tmgmt.data')->filterData($item)) {
        $cdt_character_count = $this->get('cdt_character_count')->value;
        $cdt_character_count += TmgmtCdtFormatHelper::characterCount($item['#text']);
        $this->set('cdt_character_count', $cdt_character_count);

        $cdt_settings = Json::decode($this->get('cdt_settings')->value);
        if (!isset($cdt_settings['links'])) {
          $cdt_settings['links'] = [];
        }
        $cdt_settings['links'] = array_merge($cdt_settings['links'], TmgmtCdtFormatHelper::extractHrefs($item['#text']));
        $this->set('cdt_settings', Json::encode($cdt_settings));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function recalculateStatistics() {

    if (!$this->isAccepted()) {
      $this->set('cdt_character_count', 0);
    }
    parent::recalculateStatistics();
  }

  /**
   * {@inheritdoc}
   */
  public function getJob() {
    $job = parent::getJob();
    if (!$job && $this->onCart) {
      return new FakeTmgmtCdtJob();
    }
    if ($job) {
      $job->set('target_language', $this->get('cdt_target_language')->value);
    }

    return $job;
  }

}
