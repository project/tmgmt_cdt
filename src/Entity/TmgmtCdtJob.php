<?php

namespace Drupal\tmgmt_cdt\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Language\Language;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt_cdt\TmgmtCdtHelper;

/**
 * Override class Job.
 */
class TmgmtCdtJob extends Job {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function label($langcode = NULL) {
    $label = parent::label($langcode);
    if (empty($label)) {
      return 'Translation job ' . (string) $this->get('tjid')->value;
    }
    else {
      return $label;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getConflictingItems() {
    return [];
  }

  /**
   * Create all job items.
   */
  public function jobItemGenerateTargetLanguages() {

    $target_languages = array_values($this->getSetting('cdt')['target_languages']);
    foreach ($this->getItems() as $job_item) {

      for ($i = 0; $i < \count((array) $target_languages); $i++) {
        if ($i === 0) {
          // First lang for the current item.
          $job_item->set('cdt_target_language', $target_languages[$i]);
          $job_item->save();
        }
        else {
          // Create duplicates items for other languages.
          $clone_job_item = [];
          $clone_job_item['tjid'] = $job_item->tjid;
          $clone_job_item['data'] = $job_item->get('data')->value;
          $clone_job_item['cdt_source_language'] = $job_item->get('cdt_source_language')->value;
          $clone_job_item['cdt_target_language'] = $target_languages[$i];
          $clone_job_item['cdt_character_count'] = 0;
          $clone_job_item['cdt_settings'] = $job_item->get('cdt_settings')->value;
          $clone_job_item['state'] = JobItemInterface::STATE_ACTIVE;

          $item = tmgmt_job_item_create($job_item->plugin, $job_item->get('item_type')->value, $job_item->get('item_id')->value, $clone_job_item);

          $item->save();
        }
      }
    }
  }

  /**
   * If has items with translations to review.
   */
  public function hasItemsToReview() {
    $needReview = FALSE;
    foreach ($this->getItems() as $job_item) {
      if ($job_item->isNeedsReview()) {
        $needReview = TRUE;
        break;
      }
    }
    return $needReview;
  }

  /**
   * Adds an item to the translation job.
   */
  public function addItem($plugin, $item_type, $item_id) {

    $transaction = \Drupal::database()->startTransaction();
    $is_new = FALSE;

    if ($this->isNew()) {
      $this->save();
      $is_new = TRUE;
    }

    $item = tmgmt_job_item_create(
          $plugin, $item_type, $item_id, [
            'tjid' => $this->id(),
            'cdt_source_language' => $this->get('source_language')->value,
          ]
      );
    $item->save();
    if ($item->getWordCount() == 0) {
      $transaction->rollback();

      // In case we got word count 0 for the first job item, NULL tjid so that
      // if there is another addItem() call the rolled back job object will get
      // persisted.
      if ($is_new) {
        $this->tjid = NULL;
      }

      throw new TMGMTException(
            'Job item @label (@type) has no translatable content.',
            ['@label' => $item->label(), '@type' => $item->getSourceType()]
        );
    }

    return $item;

  }

  /**
   * {@inheritdoc}
   */
  public function addTranslatedDataLanguage(array $data, $target_language) {
    $job_items = $this->getItemsLanguage($target_language);
    $count = 0;

    foreach ($job_items as $job_item) {
      if ($job_item->isNeedsReview()) {
        continue;
      }
      $count++;
      foreach ($data as $key => $value) {
        if ($job_item->get('item_id')->value == $key) {
          $job_item->addTranslatedData($value);
        }
      }
    }

    return $count;
  }

  /**
   * Update job items status on new pickup.
   */
  public function pickupPrepareJobItems() {

    $cdt_settings = @Json::decode($this->get('cdt_settings')->value);
    if (isset($cdt_settings['on_pickup'])) {
      return;
    }

    foreach ($this->getItems() as $job_item) {
      if ($job_item->isNeedsReview()) {
        $job_item->active();
        $job_item->save();
      }
    }

    $cdt_settings['on_pickup'] = TRUE;
    $this->set('cdt_settings', Json::encode($cdt_settings));
    $this->save();

  }

  /**
   * Same as parent::getItems but with language.
   */
  public function getItemsLanguage($target_language) {

    $items = [];
    $query = \Drupal::entityQuery('tmgmt_job_item')
      ->accessCheck(FALSE)
      ->condition('tjid', $this->id())
      ->condition('cdt_target_language', $target_language);

    $results = $query->execute();
    if (!empty($results)) {
      $items = \Drupal::entityTypeManager()->getStorage('tmgmt_job_item')->loadMultiple($results);
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetLanguage() {
    if ($this->get('target_language') === 'multiple') {
      $lang = [
        'name' => 'Multiple',
        'id' => 'multiple',
        'direction' => 'ltr',
        'locked' => FALSE,
      ];
      return new Language($lang);
    }
    else {
      return \Drupal::languageManager()->getDefaultLanguage();
    }

  }

  /**
   * Add more indicators.
   */
  public function getCounts() {
    $count = [];
    foreach ($this->getItems() as $item) {
      if (!isset($count[$item->get('cdt_target_language')->value][$item->get('item_id')->value])) {
        $count[$item->get('cdt_target_language')->value][$item->get('item_id')->value] = 0;
      }
      $count[$item->get('cdt_target_language')->value][$item->get('item_id')->value] += $item->get('cdt_character_count')->value;
    }

    if (\count($count) > 0) {
      $languages = \count(array_keys($count));
      $characters_language = (int) array_sum(reset($count));
      $pages_language = ceil($characters_language / 750) * 0.5;
      $items = \count(reset($count));
    }
    else {
      $languages = 0;
      $characters_language = 0;
      $pages_language = 0;
      $items = 0;

    }

    $out = [
      'languages' => $languages,
      'characters' => $characters_language * $languages,
      'characters_language' => $characters_language,
      'items' => $items,
      'pages' => $pages_language * $languages,
      'pages_language' => $pages_language,
    ];

    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function isAbortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function acceptTranslation() {
    $items_max = (int) TmgmtCdtHelper::getState('job_max_process');
    $items_count = 0;

    $job_counts = $this->getCounts();
    $total_items = (int) $job_counts['items'] * (int) $job_counts['languages'];
    unset($job_counts);

    $options = [
      'state' => JobItemInterface::STATE_REVIEW,
    ];
    foreach ($this->getItems($options) as $item) {

      if ($items_count >= $items_max) {
        \Drupal::messenger()->addWarning($this->t('This job is big (@total_items items), @items_count done, please do it again for the remaining items',
              [
                '@total_items' => $total_items,
                '@items_count' => $items_count,
              ]));
        break;
      }
      if ($item->acceptTranslation()) {
        $items_count++;
      }
    }

    if (tmgmt_job_check_finished($this->id())) {
      $this->setState(JobInterface::STATE_FINISHED);
      $this->set('target_language', 'multiple');
      $this->save();
      $this->addMessage($this->t('The translation job has been finished.'));
    }
  }

}

/**
 * To use JobItem->getSourceData() from items in cart.
 *
 * They are not yet linked to a parent job.
 */
class FakeTmgmtCdtJob {

  /**
   * {@inheritdoc}
   */
  public function getSourceLangcode() {
    return \Drupal::languageManager()->getDefaultLanguage()->getId();
  }

}
