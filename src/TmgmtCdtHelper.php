<?php

namespace Drupal\tmgmt_cdt;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Site\Settings;
use Drupal\tmgmt_cdt\Entity\TmgmtCdtJobItem;

/**
 * Helper class.
 */
class TmgmtCdtHelper {

  const SIMULATION = 'SIMULATION';
  const PRODUCTION = 'PRODUCTION';

  const NAME = 'Translation Centre (CdT)';
  const DEFAULT_REQUEST_IDENTIFIER = '0000/unknown';
  const TOKEN_TYPE_DRUPAL_TOKEN_MEDIA = 'drupal_token_media';
  const TOKEN_TYPE_DRUPAL_TOKEN_SIMPLE = 'drupal_token_simple';
  const TOKEN_TYPE_DRUPAL_TOKEN_YAML = 'drupal_token_yaml';
  const TOKEN_TYPE_HTML_A = 'html_a';
  const TOKEN_TYPE_HTML_IMG = 'html_img';
  const TOKEN_TYPE_HTML_SCRIPT = 'html_script';

  const API_STATUS_COMPLETED = 'COMPLETED';
  const API_STATUS_QUOTED_PENDING_APPROVAL = 'QUOTED - PENDING APPROVAL';
  const API_STATUS_CANCELLED = 'CANCELLED';

  const API_COMMENT_URLS = [
    'DO_NOT_ADAPT_EXTERNAL_URLS' => 'Do not adapt external urls',
    'ADAPT_EXTERNAL_URLS' => 'Adapt external urls',
  ];

  const API_PRODUCTION_URL = 'https://b2becdt.cdt.europa.eu/generic-rest/';

  /**
   * Check if is a json.
   */
  public static function isJson($data) {

    try {
      // Must be \json_decode function.
      if (\is_string($data) && \is_array(\json_decode($data, TRUE)) && (json_last_error() === JSON_ERROR_NONE)) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    catch (\Exception $e) {
      return FALSE;
    }

  }

  /**
   * Get default options.
   */
  public static function defaultOptions() {

    $options = [

      'tmgmt_cdt.api_mode' => self::SIMULATION,
      'tmgmt_cdt.ready' => FALSE,
      'tmgmt_cdt.debug' => FALSE,

      'tmgmt_cdt.business_reference_data' => new \stdClass(),
      'tmgmt_cdt.api_url' => '',
      'tmgmt_cdt.api_username' => '',
      'tmgmt_cdt.api_password' => '',

      'tmgmt_cdt.curl_proxy_host' => '',
      'tmgmt_cdt.curl_proxy_port' => '',
      'tmgmt_cdt.curl_proxy_user' => '',
      'tmgmt_cdt.curl_proxy_pass' => '',
      'tmgmt_cdt.curl_proxy_user_agent' => '',
      'tmgmt_cdt.curl_options' => '{"CURLOPT_TIMEOUT":15,"CURLOPT_RETURNTRANSFER":true,"CURLOPT_SSL_VERIFYHOST":0,"CURLOPT_SSL_VERIFYPEER":0}',

      'tmgmt_cdt.max_pages_per_request' => 10,
      'tmgmt_cdt.use_token_html_a' => FALSE,
      'tmgmt_cdt.use_token_html_img' => FALSE,
      'tmgmt_cdt.use_token_html_script' => TRUE,
      'tmgmt_cdt.use_token_drupal' => TRUE,
      'tmgmt_cdt.html_check' => TRUE,
      'tmgmt_cdt.quotation' => FALSE,
      'tmgmt_cdt.xsd_link' => 'http://static.cdt.europa.eu/webtranslations/schemas/Drupal-Translation-V8-2.xsd',
      'tmgmt_cdt.disable_xsd_validation' => FALSE,
      'tmgmt_cdt.xsd_source' => '',

      'tmgmt_cdt.api_access_token_expire' => 0,
      'tmgmt_cdt.api_access_token' => '',

      'tmgmt_cdt.exclude_fields' => '',
      'tmgmt_cdt.disable_simulator_injection' => FALSE,

      'tmgmt_cdt.job_max_items' => 30,
      'tmgmt_cdt.job_max_process' => 300,

    ];

    return $options;
  }

  /**
   * Get default options.
   */
  public static function getState($name) {
    try {
      if (!isset(self::defaultOptions()["tmgmt_cdt.$name"])) {
        throw new \Exception("Unknown tmgmt_cdt.$name option");
      }
      return \Drupal::state()->get("tmgmt_cdt.$name", self::defaultOptions()["tmgmt_cdt.$name"]);
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return FALSE;
    }
  }

  /**
   * File get content using translator proxy settings.
   */
  public static function curlGetFileContents($url, $options = []) {
    try {

      $proxy_host = trim((string) self::getState('curl_proxy_host'));
      $proxy_port = (int) self::getState('curl_proxy_port');
      $proxy_user = trim((string) self::getState('curl_proxy_user'));
      $proxy_pass = trim((string) self::getState('curl_proxy_pass'));
      $proxy_user_agent = trim((string) self::getState('curl_proxy_user_agent'));
      $curl_options = Json::decode(self::getState('curl_options'));

      $curl = curl_init();
      $curlopt[CURLOPT_RETURNTRANSFER] = 1;
      $curlopt[CURLOPT_URL] = $url;

      if (!empty($proxy_host)) {
        $curlopt[CURLOPT_PROXY] = $proxy_host;
        $curlopt[CURLOPT_PROXYPORT] = $proxy_port;
      }

      if (!empty($proxy_user)) {
        $curlopt[CURLOPT_PROXYUSERPWD] = $proxy_user . ':' . $proxy_pass;
      }

      if (!empty($proxy_user_agent)) {
        $curlopt[CURLOPT_USERAGENT] = $proxy_user_agent;
      }

      foreach ($curl_options as $curl_option => $curl_value) {
        $curlopt[constant($curl_option)] = $curl_value;
      }

      foreach ($options as $curl_option => $curl_value) {
        $curlopt[constant($curl_option)] = $curl_value;
      }

      curl_setopt_array($curl, $curlopt);
      $raw_data = curl_exec($curl);
      if (curl_errno($curl)) {
        throw new \Exception(curl_error($curl));
      }

      curl_close($curl);
      unset($curl);

      if ($raw_data) {
        return $raw_data;
      }
      else {
        return FALSE;
      }

    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return FALSE;
    }

  }

  /**
   * Show if the module status is ready or not.
   */
  public static function showStatus($showReady = FALSE) {
    if ($showReady) {
      if (!(bool) self::getState('ready')) {
        \Drupal::messenger()->addWarning(t('<strong>@name</strong> translator is <strong>not ready</strong>.', ['@name' => self::NAME]));
      }
      else {
        \Drupal::messenger()->addStatus(t('<strong>@name</strong> translator is <strong>ready</strong>.', ['@name' => self::NAME]));
      }
    }

    if (self::getState('api_mode') === self::SIMULATION && (bool) self::getState('ready')) {
      \Drupal::messenger()->addWarning(
            t(
                '<strong>@name</strong> use the mode <strong>@mode</strong>.',
                [
                  '@name' => self::NAME,
                  '@mode' => self::getState('api_mode'),
                ]
            )
        );
    }

  }

  /**
   * Get api credentials (user, pass and webservice url).
   */
  public static function getCredentials() {
    $credentials = Settings::get('tmgmt_cdt', FALSE);
    if (!$credentials) {
      $credentials = [
        'api_url' => self::getState('api_url'),
        'api_username' => self::getState('api_username'),
        'api_password' => self::getState('api_password'),
        'source' => 'state',
      ];
    }
    else {
      $credentials['source'] = 'settings';
    }

    return $credentials;
  }

  /**
   * Extract year and number.
   */
  public static function splitRequestIdentifier(string $request_number) {
    $request_ids = explode('/', trim((string) $request_number));
    $request_year = @trim((string) $request_ids[0]);
    $request_number = @trim((string) $request_ids[1]);
    return [
      'requestyear' => $request_year,
      'requestnumber' => $request_number,
    ];
  }

  /**
   * Check if translated xml have the same structure without CDATA content.
   */
  public static function isSameXmlStructure(string $xml_source, string $xml_target) {
    // Remove CDATA content.
    $xml_source = preg_replace('/<!\[CDATA\[[\s\S]*?\]\]>/m', '', trim((string) $xml_source));
    $xml_target = preg_replace('/<!\[CDATA\[[\s\S]*?\]\]>/m', '', trim((string) $xml_target));
    return $xml_source === $xml_target;
  }

  /**
   * Delete array key by path.
   */
  public static function arrayDeleteByPath(array &$data, string $path = '', string $separator = '][') {
    $segments = explode($separator, $path);
    $size = \count((array) $segments);
    $cur = &$data;
    $count = 0;
    foreach ($segments as $segment) {
      $count++;
      if (!isset($cur[$segment])) {
        return NULL;
      }
      if ($count === $size) {
        if (isset($cur[$segment])) {
          unset($cur[$segment]);
          return NULL;
        }
      }
      $cur = &$cur[$segment];
    }
  }

  /**
   * Check job rules.
   */
  public static function checkJob($job) {
    $errors = [];
    try {
      $volume = (int) $job->getCounts()['pages_language'];
      $max = (int) self::getState('max_pages_per_request');
      if ($volume > $max) {
        $errors[] = t(
              'The translation volume is too large (@volume pages). The maximum per request is @max pages.',
              ['@volume' => $volume, '@max' => $max]
          );
      }

      // @todo check if all job items have the same source language
      return $errors;
    }
    catch (\Exception $e) {
      $errors[] = $e->getMessage();
      return $errors;
    }
  }

  /**
   * Check job items.
   */
  public static function checkCartItems(array $items) {
    $errors = [];
    $characters = 0;
    $volume = 0;
    $max_pages = (int) self::getState('max_pages_per_request');

    $max_items = (int) self::getState('job_max_items');
    $items_count = 0;

    $job_items = TmgmtCdtJobItem::loadMultiple($items);
    foreach ($job_items as $job_item) {
      if (!$job_item) {
        \Drupal::messenger()->addError('errors on cart validation');
        continue;
      }
      $items_count++;
      // To activate the FakeTmgmtCdtJob Class on next functions.
      $job_item->onCart = TRUE;

      foreach (\Drupal::service('tmgmt.data')->filterTranslatable($job_item->getSourceData()) as $data) {
        $characters += TmgmtCdtFormatHelper::characterCount(@$data['#text']);
      }

      // Back to FALSE, to not use FakeTmgmtCdtJob anymore.
      $job_item->onCart = FALSE;
    }

    $volume = ceil($characters / 750) * 0.5;
    if ($volume > $max_pages) {
      $errors[] = t(
            'The translation volume is too large (@volume pages). The maximum per request is @max_pages pages.',
            ['@volume' => $volume, '@max_pages' => $max_pages]
        );
    }

    if ($items_count > $max_items) {
      $errors[] = t(
            'Too many items: @items_count. The maximum per request is @max_items items.',
            ['@items_count' => $items_count, '@max_items' => $max_items]
        );
    }
    return $errors;
  }

  /**
   * Return job by request_identifier.
   */
  public static function jobByRequestIdentifier(string $request_identifier) {
    if ($request_identifier === self::DEFAULT_REQUEST_IDENTIFIER) {
      return FALSE;
    }

    $query = \Drupal::entityQuery('tmgmt_job')
      ->accessCheck(FALSE)
      ->condition('translator', 'tmgmt_cdt')
      ->condition('cdt_request_identifier', $request_identifier);
    $result = $query->execute();
    if (empty($result)) {
      return FALSE;
    }

    $ids = array_keys($result);
    $job = \Drupal::entityTypeManager()->getStorage('tmgmt_job')->load((int) reset($ids));

    return $job;
  }

  /**
   * Get fields list to exclude.
   */
  public static function getFieldsToExclude() {
    $fields_exclude_list = explode(';', trim((string) self::getState('exclude_fields')));
    // Clean items.
    for ($i = 0; $i < \count((array) $fields_exclude_list); $i++) {
      $fields_exclude_list[$i] = trim((string) $fields_exclude_list[$i]);
    }
    return $fields_exclude_list;

  }

}
