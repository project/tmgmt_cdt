<?php

namespace Drupal\tmgmt_cdt;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\JobInterface;

/**
 * @file
 * CDT WebService singleton.
 * @usage get instance with: TmgmtCdtApi::getInstance();.
 */

/**
 * Class sigleton webservice client.
 */
class TmgmtCdtApi {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Instance.
   *
   * @var null
   */
  private static $instance = NULL;

  /**
   * Baseurl.
   *
   * @var string
   */
  private $baseUrl;

  /**
   * ProxyHost.
   *
   * @var string
   */
  private $proxyHost;

  /**
   * ProxyPort.
   *
   * @var string
   */
  private $proxyPort;

  /**
   * ProxyUser.
   *
   * @var string
   */
  private $proxyUser;

  /**
   * ProxyPass.
   *
   * @var string
   */
  private $proxyPass;

  /**
   * ProxyUserAgent.
   *
   * @var string
   */
  private $proxyUserAgent;
  /**
   * CurlOptions.
   *
   * @var string
   */
  private $curlOptions;

  /**
   * Username.
   *
   * @var string
   */
  private $username;

  /**
   * Username.
   *
   * @var string
   */
  private $password;

  /**
   * IsReady.
   *
   * @var bool
   */
  private $isReady;

  /**
   * Method.
   *
   * @var string
   */
  private $method;

  /**
   * Response.
   *
   * @var string
   */
  public $response;

  /**
   * FakeResponse.
   *
   * @var string
   */
  private $fakeResponse;

  /**
   * HasDebug.
   *
   * @var string
   */
  private $hasDebug;

  /**
   * OriginalActionPath.
   *
   * @var string
   */
  private $originalActionPath;

  /**
   * ActionPath.
   *
   * @var string
   */
  private $actionPath;

  /**
   * Url.
   *
   * @var string
   */
  public $url;

  /**
   * ParamsPath.
   *
   * @var string
   */
  private $paramsPath;

  /**
   * ParamsQuery.
   *
   * @var string
   */
  private $paramsQuery;

  /**
   * ParamsBody.
   *
   * @var string
   */
  private $paramsBody;

  /**
   * ParamsForm.
   *
   * @var string
   */
  private $paramsForm;

  /**
   * Uniqid.
   *
   * @var string
   */
  public $uniqid;

  /**
   * AccessToken.
   *
   * @var string
   */
  private $accessToken;

  /**
   * ApiMode.
   *
   * @var string
   */
  private mixed $apiMode;

  /**
   * HttpCodes.
   *
   * @var array
   */
  private $httpCodes = [
    100 => 'Continue',
    101 => 'Switching Protocols',
    102 => 'Processing',
    103 => 'Checkpoint',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    207 => 'Multi-Status',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => 'Switch Proxy',
    307 => 'Temporary Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Long',
    415 => 'Unsupported Media Type',
    416 => 'Requested Range Not Satisfiable',
    417 => 'Expectation Failed',
    418 => 'I\'m a teapot',
    422 => 'Unprocessable Entity',
    423 => 'Locked',
    424 => 'Failed Dependency',
    425 => 'Unordered Collection',
    426 => 'Upgrade Required',
    449 => 'Retry With',
    450 => 'Blocked by Windows Parental Controls',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'HTTP Version Not Supported',
    506 => 'Variant Also Negotiates',
    507 => 'Insufficient Storage',
    509 => 'Bandwidth Limit Exceeded',
    510 => 'Not Extended',
  ];

  /**
   * List of simulated api calls.
   *
   * @var array
   */
  private $simulatedCalls = [
    'GET/requests/requestIdentifierByCorrelationId/{correlationID}',
    'GET/requests/{requestyear}/{requestnumber}/targets-base64',
    'GET/requests/{requestyear}/{requestnumber}/iscomplete',
    'POST/requests',
    'GET/requests/{requestyear}/{requestnumber}',
    'POST/requests/validate',
    'POST/token',
    'GET/CheckConnection',
    'GET/requests/businessReferenceData',
    'POST//requests/{requestyear}/{requestnumber}/CancelQuotation',
    'POST/requests/{requestyear}/{requestnumber}/ApproveQuotation',
    'GET/requests/{requestyear}/{requestnumber}/QuotationSummary',
  ];

  public function __construct() {

    $credentials = TmgmtCdtHelper::getCredentials();

    $this->uniqid = uniqid();
    $this->setMode(TmgmtCdtHelper::getState('api_mode'));
    $this->baseUrl = $credentials['api_url'];
    $this->proxyHost = trim((string) TmgmtCdtHelper::getState('curl_proxy_host'));
    $this->proxyPort = (int) TmgmtCdtHelper::getState('curl_proxy_port');
    $this->proxyUser = trim((string) TmgmtCdtHelper::getState('curl_proxy_user'));
    $this->proxyPass = trim((string) TmgmtCdtHelper::getState('curl_proxy_pass'));
    $this->proxyUserAgent = trim((string) TmgmtCdtHelper::getState('curl_proxy_user_agent'));
    $this->username = $credentials['api_username'];
    $this->password = base64_decode($credentials['api_password']);
    $this->setReady((bool) TmgmtCdtHelper::getState('ready'));
    $this->accessToken = TmgmtCdtHelper::getState('api_access_token');
    try {
      $this->curlOptions = Json::decode(TmgmtCdtHelper::getState('curl_options'));
    }
    catch (\Exception $e) {
      $this->isReady = FALSE;
      $this->messenger()->addError($e->getMessage());
    }

  }

  /**
   * Change mode to PRODUCTION or SIMULATION.
   */
  public function setMode($mode = TmgmtCdtHelper::SIMULATION) {
    if (!in_array(
          $mode, [TmgmtCdtHelper::SIMULATION,
            TmgmtCdtHelper::PRODUCTION,
          ]
      )
      ) {
      $this->apiMode = TmgmtCdtHelper::SIMULATION;
    }
    else {
      $this->apiMode = $mode;
    }
  }

  /**
   * Get mode: PRODUCTION or SIMULATION.
   */
  public function getMode() {
    return $this->apiMode;
  }

  /**
   * Get singleton instance.
   */
  public static function getInstance() {
    if (self::$instance === NULL) {
      self::$instance = new TmgmtCdtApi();
    }
    self::$instance->resetInstance();
    self::$instance->checkTokenAccess();
    self::$instance->resetInstance();

    return self::$instance;
  }

  /**
   * Clean all variables.
   */
  public function resetInstance() {

    $this->setMode(TmgmtCdtHelper::getState('api_mode'));
    $this->setDebug((bool) TmgmtCdtHelper::getState('debug'));
    $this->response = new \stdClass();
    $this->response->code = 400;
    $this->response->status = 'fail';
    $this->response->content = NULL;
    $this->response->errors = [];
    $this->fakeResponse = NULL;
    $this->url = "";
    $this->method = "GET";
    $this->actionPath = "";
    $this->originalActionPath = "";
    $this->paramsPath = [];
    $this->paramsQuery = [];
    $this->paramsBody = new \stdClass();
    $this->paramsForm = [];

  }

  /**
   * SetDebug.
   */
  public function setDebug(bool $state) {
    $this->hasDebug = (bool) $state;
  }

  /**
   * SetReady.
   */
  public function setReady(bool $state) {
    $this->isReady = (bool) $state;
  }

  /**
   * SsetParamsPath: params by url paths.
   */
  public function setParamsPath(array $params) {
    $this->paramsPath = (array) $params;
  }

  /**
   * SetParamsQuery: params by queryString.
   */
  public function setParamsQuery(array $params) {
    $this->paramsQuery = (array) $params;
  }

  /**
   * SetParamsBody: params inside body (POST).
   */
  public function setParamsBody($params) {
    $this->paramsBody = $params;
  }

  /**
   * SetParamsForm: params form data (POST).
   */
  public function setParamsForm($params) {
    $this->paramsForm = $params;
  }

  /**
   * Use this response on next call.
   *
   * Ex format {'code': 200, 'status':'success', 'content': any}.
   */
  public function setFakeResponse($response) {
    $this->fakeResponse = $response;
  }

  /**
   * Return a custom response.
   */
  private function generateFakeResponse() {

    $this->response->code = (int) $this->fakeResponse->code;
    $this->response->status = $this->fakeResponse->status === 'success' ? 'success' : 'fail';
    $this->response->content = $this->fakeResponse->content;
    $this->fakeResponse = NULL;

  }

  /**
   * Post.
   */
  public function post($actionPath) {
    $this->method = 'POST';
    $this->originalActionPath = $actionPath;
    if (\count((array) $this->paramsPath) > 0) {
      foreach ($this->paramsPath as $id => $val) {
        $actionPath = str_replace('{' . trim((string) $id) . '}', trim((string) $val), $actionPath);
      }
    }
    return $this->call(trim((string) $actionPath));
  }

  /**
   * Get.
   */
  public function get($actionPath) {
    $this->method = 'GET';
    $this->originalActionPath = $actionPath;
    if (\count((array) $this->paramsPath) > 0) {
      foreach ($this->paramsPath as $id => $val) {
        $actionPath = str_replace('{' . trim((string) $id) . '}', trim((string) $val), $actionPath);
      }
    }
    return $this->call(trim((string) $actionPath));
  }

  /**
   * Call.
   */
  private function call($actionPath) {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->actionPath = $actionPath;
      $this->url = $this->baseUrl;
      $this->url .= $actionPath;

      if (\count((array) $this->paramsQuery) > 0) {
        $this->url .= "?" . UrlHelper::buildQuery($this->paramsQuery);

      }

      if ($this->hasDebug) {
        $this->response->debug = [
          'uniqid' => $this->uniqid,
          'url' => $this->url,
          'originalActionPath' => $this->originalActionPath,
          'actionPath' => $this->actionPath,
          'simulatorUsed' => $this->getMode() === TmgmtCdtHelper::SIMULATION,
          'method' => $this->method,
          'paramsQuery' => $this->paramsQuery,
          'paramsQueryJson' => Json::encode($this->paramsQuery),
          'paramsPath' => $this->paramsPath,
          'paramsPathJson' => Json::encode($this->paramsPath),
          'paramsBody' => $this->paramsBody,
          'paramsBodyJson' => Json::encode($this->paramsBody),
          'paramsForm' => $this->paramsForm,
          'paramsFormJson' => Json::encode($this->paramsForm),
          'token_access' => TmgmtCdtHelper::getState('api_access_token'),
          'token_access expire' => TmgmtCdtHelper::getState('api_access_token_expire'),
          'username' => $this->username,
          'password' => $this->password,
          'proxyHost' => $this->proxyHost,
          'proxyPort' => $this->proxyPort,
          'proxyUser' => $this->proxyUser,
          'proxyPass' => $this->proxyPass,
          'proxyUserAgent' => $this->proxyUserAgent,
        ];
      }

      if (!$this->isReady) {
        throw new \Exception('Translator ' . TmgmtCdtHelper::NAME . ' is not ready.');
      }

      if ($this->getMode() === TmgmtCdtHelper::SIMULATION) {
        return $this->apiSimulator();
      }

      $curl = curl_init();

      $curlopt = [];
      $curlopt[CURLOPT_HTTPHEADER] = [];

      array_push($curlopt[CURLOPT_HTTPHEADER], 'Accept: application/json');

      if ($this->actionPath !== 'token') {
        array_push($curlopt[CURLOPT_HTTPHEADER], 'Authorization: Bearer ' . $this->accessToken);
      }

      if (!empty((array) $this->paramsBody)) {
        $payload = \json_encode($this->paramsBody);
        $curlopt[CURLOPT_POSTFIELDS] = $payload;
        array_push($curlopt[CURLOPT_HTTPHEADER], 'Content-Type: application/json');
      }
      if (!empty((array) $this->paramsForm)) {
        $payload = http_build_query($this->paramsForm);
        $curlopt[CURLOPT_POSTFIELDS] = $payload;
        array_push($curlopt[CURLOPT_HTTPHEADER], 'Content-Type: application/x-www-form-urlencoded');
      }
      if (($this->method === 'POST') && empty((array) $this->paramsBody) && empty((array) $this->paramsForm)) {
        array_push($curlopt[CURLOPT_HTTPHEADER], 'Content-Length: 0');
      }

      $curlopt[CURLOPT_URL] = $this->url;

      if (!empty($this->proxyHost)) {
        $curlopt[CURLOPT_PROXY] = $this->proxyHost;
        $curlopt[CURLOPT_PROXYPORT] = $this->proxyPort;
      }
      if (!empty($this->proxyUser)) {
        $curlopt[CURLOPT_PROXYUSERPWD] = $this->proxyUser . ':' . $this->proxyPass;
      }

      if (!empty($this->proxyUserAgent)) {
        $curlopt[CURLOPT_USERAGENT] = $this->proxyUserAgent;
      }

      $curlopt[CURLOPT_CUSTOMREQUEST] = $this->method;
      // More options from module settings.
      foreach ($this->curlOptions as $curl_option => $curl_value) {
        $curlopt[constant($curl_option)] = $curl_value;
      }

      curl_setopt_array($curl, $curlopt);
      $raw_data = curl_exec($curl);

      // Get curl info for http code.
      $curl_info = curl_getinfo($curl);

      $this->response->code = @(int) $curl_info['http_code'];

      if ($this->hasDebug) {
        $this->response->debug['responseRaw'] = $raw_data;
        $this->response->debug['curlInfo'] = $curl_info;
        $this->response->debug['curlOptions'] = $curlopt;
        $this->response->debug['curlVersion'] = curl_version();
      }
      $curl_info = NULL;

      // Get curl error.
      if (curl_errno($curl)) {
        throw new \Exception(curl_error($curl));
      }

      $firewall_error = $this->isFirewallRejection($raw_data);
      if ($firewall_error !== FALSE) {
        throw new \Exception((string) $firewall_error);
      }

      if (TmgmtCdtHelper::isJson($raw_data)) {

        // Need a native json format.
        $this->response->content = \json_decode($raw_data);

      }
      else {
        $this->response->content = $raw_data;
      }
      unset($raw_data);

      if (($this->response->code < 200) || ($this->response->code > 299)) {
        if (!empty($this->response->content->errors)) {
          $_messages = [];
          $_messages[] = @$this->response->content->message;
          $_errors = @$this->response->content->errors;
          foreach ($_errors as $error) {
            $_messages[] = $error[0];
          }
          throw new \Exception(\strip_tags(implode(', ', $_messages)));
        }
        else {
          if (isset($this->httpCodes[$this->response->code])) {
            throw new \Exception($this->httpCodes[$this->response->code]);
          }
          else {
            throw new \Exception('Unknown error');
          }
        }
      }

      $this->response->status = 'success';
      curl_close($curl);
      unset($curl);

      if ($this->hasDebug) {
        \Drupal::logger('tmgmt_cdt')->debug('API success <pre><code>' . print_r($this->response, TRUE) . '</code></pre>');
      }

      return $this->response;
    }
    catch (\Exception $e) {

      $this->response->status = 'fail';
      $this->response->errors[] = \strip_tags($e->getMessage());
      $this->messenger()->addError(implode(', ', $this->response->errors));
      if ($this->hasDebug) {
        \Drupal::logger('tmgmt_cdt')->error('API fail <pre><code>' . print_r($this->response, TRUE) . '</code></pre>');
      }

      if (isset($curl)) {
        @curl_close($curl);
        unset($curl);
      }
      return $this->response;
    }
  }

  /**
   * Simulated calls.
   */
  private function apiSimulator() {
    $apiCall = $this->method . '/' . $this->originalActionPath;

    switch ($apiCall) {
      case 'GET/requests/requestIdentifierByCorrelationId/{correlationID}':
        $this->simulatedGetRequestsRequestIdentifierByCorrelationIdCorrelationId();
        break;

      case 'GET/requests/{requestyear}/{requestnumber}/targets-base64':
        $this->simulatedGetRequestsRequestyearRequestnumberTargetsBase64();
        break;

      case 'GET/requests/{requestyear}/{requestnumber}/iscomplete':
        $this->simulatedGetRequestsRequestyearRequestnumberIscomplete();
        break;

      case 'POST/requests/validate':
        $this->simulatedPostRequestsValidate();
        break;

      case 'POST/requests':
        $this->simulatedPostRequests();
        break;

      case 'GET/requests/{requestyear}/{requestnumber}':
        $this->simulatedGetRequestsRequestyearRequestnumber();
        break;

      case 'POST/token':
        $this->simulatedPostToken();
        break;

      case 'GET/CheckConnection':
        $this->simulatedGetCheckConnection();
        break;

      case 'GET/requests/businessReferenceData':
        $this->simulatedGetRequestsBusinessReferenceData();
        break;

      case 'GET/requests/{requestyear}/{requestnumber}/quotationSummary':
        $this->simulatedGetRequestsRequestyearRequestnumberQuotationSummary();
        break;

      case 'POST/requests/{requestyear}/{requestnumber}/ApproveQuotation':
        $this->simulatedPostRequestsRequestyearRequestnumberApproveQuotation();
        break;

      case 'POST/requests/{requestyear}/{requestnumber}/CancelQuotation':
        $this->simulatedPostRequestsRequestyearRequestnumberCancelQuotation();
        break;

      default:
        $this->response->content = NULL;
        $this->response->status = 'fail';
        $this->response->errors[] = $this->t("Cannot simulate @api_call", ['@api_call' => $apiCall]);
        break;
    }

    if ($this->response->status = 'success') {
      if ($this->hasDebug) {
        $this->response->debug['responseRaw'] = $this->response->content;
        \Drupal::logger('tmgmt_cdt')->debug('API success (Simulator)<pre><code>' . print_r($this->response, TRUE) . '</code></pre>');
      }
    }
    else {
      if ($this->hasDebug) {
        $this->response->debug['responseRaw'] = $this->response->content;
        if ($this->hasDebug) {
          \Drupal::logger('tmgmt_cdt')->error('API fail (Simulator)<pre><code>' . print_r($this->response, TRUE) . '</code></pre>');
        }
      }
      $this->messenger()->addError(implode(', ', $this->response->errors));
    }
    return $this->response->content;
  }

  /**
   * Simulated GET/requests/requestIdentifierByCorrelationId/{correlationID}.
   */
  private function simulatedGetRequestsRequestIdentifierByCorrelationIdCorrelationId() {

    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      if (empty($this->paramsPath['correlationID'])) {
        throw new \Exception('correlationID is empty');
      }

      $query = \Drupal::entityQuery('tmgmt_job')
        ->accessCheck(FALSE)
        ->condition('translator', 'tmgmt_cdt')
        ->condition('cdt_correlation_id', $this->paramsPath['correlationID'])
        ->condition('cdt_correlation_id', 'SIMUL_%', 'LIKE');
      $result = $query->execute();

      if (empty($result)) {
        throw new \Exception('Invalid correlation_id');
      }

      $ids = array_keys($result);
      $job = \Drupal::entityTypeManager()->getStorage('tmgmt_job')->load((int) reset($ids));

      if (($job->get('cdt_request_identifier')->value === TmgmtCdtHelper::DEFAULT_REQUEST_IDENTIFIER) || empty($job->get('cdt_request_identifier')->value)) {
        $this->response->content = '"' . date('Y') . '/SIMUL_' . $job->id() . '"';
      }
      else {
        $this->response->content = '"' . $job->get('cdt_request_identifier')->value . '"';
      }

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated GET/requests/{requestyear}/{requestnumber}/iscomplete.
   */
  private function simulatedGetRequestsRequestyearRequestnumberIscomplete() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $request_identifier = $this->paramsPath['requestyear'] . '/' . $this->paramsPath['requestnumber'];
      $job = TmgmtCdtHelper::jobByRequestIdentifier($request_identifier);
      if (!$job) {
        throw new \Exception('Invalid request_identifier');
      }

      $this->response->content = 'true';
      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated GET/requests/{requestyear}/{requestnumber}/targets-base64.
   */
  private function simulatedGetRequestsRequestyearRequestnumberTargetsBase64() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $request_identifier = $this->paramsPath['requestyear'] . '/' . $this->paramsPath['requestnumber'];
      $job = TmgmtCdtHelper::jobByRequestIdentifier($request_identifier);
      if (!$job) {
        throw new \Exception('Invalid request_identifier');
      }

      $translator = $job->getTranslator();
      $job_source_language = $job->get('source_language')->value;
      $job_request_params = \json_decode($job->get('cdt_request_params')->value);
      $job_settings = $job->getSetting('cdt');
      unset($job);

      $source_language = $translator->mapToRemoteLanguage($job_source_language);
      $target_languages = [];
      foreach (array_values($job_settings['target_languages']) as $lang) {
        $target_languages[] = $translator->mapToRemoteLanguage($lang);
      }

      $source_document_string = base64_decode($job_request_params->sourceDocuments[0]->file->base64Data);

      $this->response->content = [];

      $count = 0;
      foreach ($target_languages as $target_language) {

        $count++;
        $dom_document = new \DomDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(TRUE);
        $dom_document->loadXML($source_document_string);
        $translation = new \stdClass();

        $translation->sourceLanguage = $source_language;
        $translation->targetLanguage = $target_language;
        $translation->fileBase64 = "";
        $translation->sourceDocument = $job_request_params->sourceDocuments[0]->file->fileName;
        $translation->fileName = str_replace(".xml", "_$target_language.xml", $job_request_params->sourceDocuments[0]->file->fileName);
        $translation->_links = new \stdClass();

        $staticContents = $dom_document->getElementsByTagName("StaticContent");
        foreach ($staticContents as $staticContent) {

          $source_content = $staticContent->nodeValue;
          $staticContent->nodeValue = '';

          if (!(bool) TmgmtCdtHelper::getState('disable_simulator_injection')) {

            preg_match_all('/([^<>]*?)(<\/?[-:\w]+(?:>|\s[^<>]*?>)|$)/m', $source_content, $html_exploded, PREG_SET_ORDER, 0);
            $html_exploded_clean = [];
            $html_exploded_count = \count((array) $html_exploded);
            foreach ($html_exploded as $text) {

              // If html, after the first tag.
              if (($html_exploded_count > 2) && (\count($html_exploded_clean) === 1)) {
                $text[0] = "($target_language) " . $text[0];
              }

              // If raw text, at the start.
              if (($html_exploded_count === 2) && (\count($html_exploded_clean) === 0)) {
                $text[0] = "($target_language) " . $text[0];
              }

              $html_exploded_clean[] = $text[0];
            }

            $translated_content = implode($html_exploded_clean);
            unset($html_exploded_clean);
          }
          else {
            $translated_content = $source_content;
          }

          $staticContent->appendChild(
                $dom_document->createCDATASection($translated_content)
            );
        }

        $translation->fileBase64 = base64_encode($dom_document->SaveXml());

        unset($dom_document);
        $this->response->content[] = $translation;
      }
      unset($source_document_string);

      $this->response->status = 'success';
      $this->response->code = 200;

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated GET/requests/businessReferenceData.
   */
  private function simulatedGetRequestsBusinessReferenceData() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $relative_path = \Drupal::service('module_handler')->getModule('tmgmt_cdt')->getPath() . '/json/businessReferenceDataDummy.json';
      $full_path = \Drupal::service('file_system')->realpath($relative_path);
      $json_contents = \file_get_contents($full_path);
      if (!$json_contents) {
        throw new \Exception('Cannot read the file: tmgmt_cdt/json/businessReferenceDataDummy.json');
      }
      $this->response->content = \json_decode($json_contents);

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }

  }

  /**
   * Simulated POST/token.
   */
  private function simulatedPostToken() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->response->content = new \stdClass();
      $this->response->content->access_token = bin2hex(openssl_random_pseudo_bytes(310));
      $this->response->content->token_type = 'bearer';
      $this->response->content->expires_in = 28799;

      $refresh_token = new \stdClass();
      $refresh_token->TokenId = bin2hex(openssl_random_pseudo_bytes(10));
      $refresh_token->Issued = gmdate('c', time());
      $refresh_token->Expires = gmdate('c', time() + 86399);
      $this->response->content->refresh_token = \json_encode($refresh_token);

      $this->response->content->username = $this->paramsForm['username'];
      $this->response->content->departmentId = '202020';
      $this->response->content->{".issued"} = gmdate('c', time());
      $this->response->content->{".expires"} = gmdate('c', time() + 28799);

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated POST/requests/{requestyear}/{requestnumber}/ApproveQuotation.
   */
  private function simulatedPostRequestsRequestyearRequestnumberApproveQuotation() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $request_identifier = $this->paramsPath['requestyear'] . '/' . $this->paramsPath['requestnumber'];
      $job = TmgmtCdtHelper::jobByRequestIdentifier($request_identifier);
      if (!$job) {
        throw new \Exception('Invalid request_identifier');
      }

      // Need a native json format.
      $request_params = \json_decode($job->get('cdt_request_params')->value);
      $request_params->isQuotationOnly = FALSE;
      $job->set('cdt_request_params', \json_encode($request_params));

      $job->save();

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated POST/requests/{requestyear}/{requestnumber}/CancelQuotation.
   */
  private function simulatedPostRequestsRequestyearRequestnumberCancelQuotation() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $request_identifier = $this->paramsPath['requestyear'] . '/' . $this->paramsPath['requestnumber'];
      if ($request_identifier === TmgmtCdtHelper::DEFAULT_REQUEST_IDENTIFIER) {
        throw new \Exception('Invalid request_identifier');
      }

      $query = \Drupal::entityQuery('tmgmt_job')
        ->accessCheck(FALSE)
        ->condition('translator', 'tmgmt_cdt')
        ->condition('cdt_request_identifier', $request_identifier);
      $result = $query->execute();

      if (empty($result)) {
        throw new \Exception('Invalid request_identifier');
      }

      $ids = array_keys($result);
      $job = \Drupal::entityTypeManager()->getStorage('tmgmt_job')->load((int) reset($ids));

      $job->setState(JobInterface::STATE_ABORTED);
      $job->save();

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated GET/requests/{requestyear}/{requestnumber}/quotationSummary.
   */
  private function simulatedGetRequestsRequestyearRequestnumberQuotationSummary() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response->content = new \stdClass();
      $this->response->content->totalPrice = 999;
      $this->response->content->jobSummary = [];

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated GET/CheckConnection.
   */
  private function simulatedGetCheckConnection() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->response->content = 'true';

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated POST/Requests/ValidateRequest.
   */
  private function simulatedPostRequestsValidate() {

    if ($this->fakeResponse !== NULL) {
      return $this->generateFakeResponse();
    }

    $this->response->content = 'true';

    $this->response->code = 200;
    $this->response->status = 'success';

    $this->response;
  }

  /**
   * Simulated POST/Requests.
   */
  private function simulatedPostRequests() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $jobId = $this->paramsBody->clientReference;
      $this->response->content = '"SIMUL_' . $jobId . '"';

      $this->response->code = 201;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Simulated GET/requests/{requestyear}/{requestnumber}.
   */
  private function simulatedGetRequestsRequestyearRequestnumber() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $request_identifier = $this->paramsPath['requestyear'] . '/' . $this->paramsPath['requestnumber'];
      $job = TmgmtCdtHelper::jobByRequestIdentifier($request_identifier);
      if (!$job) {
        throw new \Exception('Invalid request_identifier');
      }

      $translator = $job->getTranslator();
      $businessReferenceData = TmgmtCdtHelper::getState('business_reference_data');

      // Need a native json format.
      $request_params = \json_decode($job->get('cdt_request_params')->value);

      $settings = $job->getSetting('cdt');

      $this->response->content = new \stdClass();
      $this->response->content->requestIdentifier = $request_identifier;

      if ($request_params->isQuotationOnly) {
        $status = TmgmtCdtHelper::API_STATUS_QUOTED_PENDING_APPROVAL;
      }
      else {
        $status = TmgmtCdtHelper::API_STATUS_COMPLETED;
      }

      if (strpos(mb_strtolower($job->label()), 'cancelled') !== FALSE) {
        $status = TmgmtCdtHelper::API_STATUS_CANCELLED;
        $job->setState(JobInterface::STATE_ABORTED);
        $job->save();
      }

      if ($job->isAborted()) {
        $status = TmgmtCdtHelper::API_STATUS_CANCELLED;
      }

      $this->response->content->status = $status;
      $this->response->content->sourceLanguages = [$translator->mapToRemoteLanguage($job->get('source_language')->value)];

      foreach (array_values($settings['target_languages']) as $lang) {
        $this->response->content->targetLanguages[] = $translator->mapToRemoteLanguage($lang);
      }

      $this->response->content->creationDate = (new \DateTime())->setTimestamp($job->get('created')->value)->format('c');
      $this->response->content->deliveryDate = (new \DateTime())->setTimestamp($job->get('changed')->value + 259200)->format('c');
      $this->response->content->title = $job->label();
      $this->response->content->service = 'Translation';

      foreach ($businessReferenceData->departments as $dep) {
        if ($dep->code === $settings['department']) {
          $this->response->content->department = $dep->description;
        }
      }

      $this->response->content->contacts = [];
      foreach ($businessReferenceData->contacts as $user) {
        foreach (array_values($settings['contacts']) as $id) {
          if ($user->userName === $id) {
            $this->response->content->contacts[] = $user->firstName . ' ' . $user->lastName;
          }
        }
      }

      $this->response->content->deliverToContacts = [];
      foreach ($businessReferenceData->contacts as $user) {
        foreach (array_values($settings['deliver_to']) as $id) {
          if ($user->userName === $id) {
            $this->response->content->deliverToContacts[] = $user->firstName . ' ' . $user->lastName;
          }
        }
      }

      $this->response->content->sourceDocuments = [];
      $sourceDocument = new \stdClass();
      $sourceDocument->fileName = (string) $request_params->sourceDocuments[0]->file->fileName;
      $sourceDocument->_links = new \stdClass();
      $this->response->content->sourceDocuments[] = $sourceDocument;
      unset($sourceDocument);

      $this->response->content->dates = [];

      $date = new \stdClass();
      $date->label = 'Deadline';
      $date->date = (new \DateTime())->setTimestamp($job->get('changed')->value + 259200)->format('c');
      $date->ecdtDateType = 'Deadline';
      $date->tolltip = NULL;
      $this->response->content->dates[] = $date;

      $date = new \stdClass();
      $date->label = 'Latest Deadline';
      $date->date = (new \DateTime())->setTimestamp($job->get('changed')->value + 259200)->format('c');
      $date->ecdtDateType = 'LatestDeadline';
      $date->tolltip = NULL;
      $this->response->content->dates[] = $date;

      $date = new \stdClass();
      $date->label = 'Receipt date';
      $date->date = (new \DateTime())->setTimestamp($job->get('changed')->value)->format('c');
      $date->ecdtDateType = 'Receipt date';
      $date->tolltip = NULL;
      $this->response->content->dates[] = $date;
      unset($date);

      $this->response->content->comments = [];
      $comment = new \stdClass();
      $comment->comment = $request_params->comments;
      $comment->isHTML = TRUE;
      $comment->from = 'Client';
      $this->response->content->comments[] = $comment;
      unset($comment);

      $this->response->content->pricing = new \stdClass();
      $this->response->content->_links = new \stdClass();

      $this->response->code = 200;
      $this->response->status = 'success';

      $this->response;
    }
    catch (\Exception $e) {
      $this->response->status = 'fail';
      $this->response->errors[] = $e->getMessage();
      $this->response;
    }
  }

  /**
   * Check if translations are available or not.
   */
  public function isTranslationComplete($params) {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->setParamsPath($params);
      $this->get('requests/{requestyear}/{requestnumber}/iscomplete');

      if ($this->response->status === 'success') {
        // Return always a boolean TRUE or FALSE.
        return mb_strtolower($this->response->content) === 'true';
      }
      else {
        return FALSE;
      }
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * GetTranslations.
   */
  public function getTranslations($params) {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->setParamsPath($params);
      $this->get('requests/{requestyear}/{requestnumber}/targets-base64');

      if ($this->response->status === 'success') {

        if (!empty($this->response->content)) {
          for ($i = 0; $i < \count((array) $this->response->content); $i++) {
            if (!isset($this->response->content[$i]->fileBase64)) {
              $this->response->content[$i]->fileBase64 = $this->response->content[$i]->base64;
              unset($this->response->content[$i]->base64);
            }
          }
        }

        // Return an array, sometimes empty.
        return $this->response->content;
      }
      else {
        return FALSE;
      }
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Get the request_identifier from the temporary correclation_id.
   */
  public function getRequestIdentifier($correlationID) {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $params = [
        'correlationID' => $correlationID,
      ];
      $this->setParamsPath($params);
      $this->get('requests/requestIdentifierByCorrelationId/{correlationID}');

      if ($this->response->status === 'success') {
        return (string) $this->response->content;
      }
      else {
        return FALSE;
      }
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * CheckConnection.
   */
  public function checkConnection() {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->get('CheckConnection');
      return $this->response->status === 'success' && $this->response->content === 'true';

    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * IsFirewallRejection.
   */
  private function isFirewallRejection($data) {
    if (!\is_string($data)) {
      return FALSE;
    }
    preg_match_all('/(The requested URL was rejected. Please consult with your administrator.)|(Your support ID is: (\d*))/ims', $data, $matchs, PREG_SET_ORDER, 0);
    if (isset($matchs[0][0]) && isset($matchs[1][0])) {
      return $matchs[0][0] . ' ' . $matchs[1][0];
    }
    return FALSE;
  }

  /**
   * ValidateRequest.
   */
  public function validateRequest($request) {
    try {

      if ($this->fakeResponse !== NULL) {
        return $this->generateFakeResponse();
      }

      $this->setParamsBody($request);
      $this->post('requests/validate');

      if ($this->response->status === 'success' && $this->response->content === 'true') {
        return TRUE;
      }

      foreach ($this->response->errors as $error) {
        $this->messenger()->addError($error);
      }
      return FALSE;

    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Get a new token.
   */
  private function tokenPassword() {
    try {
      $params = [
        'grant_type' => 'password',
        'username' => $this->username,
        'password' => $this->password,
      ];
      $this->setParamsForm($params);
      $this->setReady(TRUE);
      $this->post('token');

      if ($this->response->status === 'success') {
        return $this->response->content;
      }
      else {
        return FALSE;
      }
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Check the token validity and ask new one if expired.
   */
  private function checkTokenAccess() {

    try {

      if (time() > (int) TmgmtCdtHelper::getState('api_access_token_expire') || empty(TmgmtCdtHelper::getState('api_access_token'))) {
        $tokenResult = $this->tokenPassword();
        if (!$tokenResult) {
          throw new \Exception('Cannot generate a new access token. Check your credentials.');
        }
        // Remove 5 mn.
        \Drupal::state()->set('tmgmt_cdt.api_access_token_expire', time() + (int) $tokenResult->expires_in - 300);
        $this->accessToken = $tokenResult->access_token;
        \Drupal::state()->set('tmgmt_cdt.api_access_token', $this->accessToken);
      }

    }
    catch (\Exception $e) {
      $this->setReady(FALSE);
      $this->messenger()->addError($e->getMessage());

    }

  }

}
