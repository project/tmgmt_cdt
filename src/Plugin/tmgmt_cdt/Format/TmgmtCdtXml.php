<?php

namespace Drupal\tmgmt_cdt\Plugin\tmgmt_cdt\Format;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_cdt\TmgmtCdtFormatHelper;
use Drupal\tmgmt_cdt\TmgmtCdtHelper;
use Drupal\tmgmt_file\Format\FormatInterface;

/**
 * Manage the xml file.
 */
class TmgmtCdtXml extends \XMLWriter implements FormatInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Contains a reference to the currently being exported job.
   *
   * @var \Drupal\tmgmt\Entity\Job
   */

  /**
   * Summary of job.
   *
   * @var mixed
   */
  protected $job;

  /**
   * Job items mapping.
   *
   * @var mixed
   */
  protected $mappedItemsIDs;

  /**
   * Xml content.
   *
   * @var mixed
   */
  protected $importedXml;

  /**
   * Adds a job item to the xml export.
   */
  protected function addItem(JobItemInterface $item) {

    $this->startElement('Translation');

    $this->startElement('PreviewLink');
    $this->writeCdata('NO PREVIEW LINK');
    $this->endElement();

    $this->startElement('SourceReference');
    $this->writeCdata(TmgmtCdtFormatHelper::itemSourceReference($item)['link']);
    $this->endElement();

    $data = \Drupal::service('tmgmt.data')->filterTranslatable($item->getData());

    // Find field(s) with "title" and put at the start.
    $titles = [];
    foreach ($data as $key => $field) {
      if (strpos(mb_strtolower($key), 'title') !== FALSE) {
        $titles[$key] = $data[$key];
        unset($data[$key]);
      }
    }
    // Titles ..
    $data_ordered = [];
    foreach ($titles as $key => $field) {
      $data_ordered[$key] = $field;
    }
    // And other ...
    foreach ($data as $key => $field) {
      $data_ordered[$key] = $field;
    }

    $cdt_settings = @Json::decode($item->get('cdt_settings')->value);
    if (!isset($cdt_settings['tagsTypeCount'])) {
      $cdt_settings['tagsTypeCount'] = [];
    }

    $fields_exclude_list = TmgmtCdtHelper::getFieldsToExclude();

    foreach ($data_ordered as $key => $field) {
      if (in_array('][' . $key, $fields_exclude_list)) {
        continue;
      }

      $this->startElement('StaticContent');
      $this->writeAttribute('indexType', $item->get('item_type')->value);
      $this->writeAttribute('instanceId', $item->get('item_id')->value);
      $this->writeAttribute('resName', '][' . $key);

      if (TmgmtCdtFormatHelper::isHtml($field['#text'])) {
        $this->writeAttribute('resType', 'html');
      }
      else {
        $this->writeAttribute('resType', 'text');
      }

      if (isset($field['#max_length'])) {
        $this->writeAttribute('resMaxSize', (int) $field['#max_length']);
      }
      else {
        $this->writeAttribute('resMaxSize', '');
      }

      $label = @$field['#parent_label'][0] ? $field['#parent_label'][0] : 'Field';
      $this->writeAttribute('resLabel', $label);

      if ($item->get('item_type')->value === 'webform_options' && $label === 'Options (YAML)') {
        $content = TmgmtCdtFormatHelper::convertWebformOptions(trim((string) $field['#text']));
      }
      else {
        $content = TmgmtCdtFormatHelper::convertDrupalToToken(trim((string) $field['#text']));
      }

      $_key = str_replace('[', '', $key);
      $_key = str_replace(']', '', $_key);

      $cdt_settings['tagsTypeCount'][$_key] = TmgmtCdtFormatHelper::tagsTypeCount($content);

      $this->writeCdata($content);
      unset($content);
      // End tag </StaticContent>.
      $this->endElement();
    }
    // End tag </Translation>.
    $this->endElement();

    $item->set('cdt_settings', Json::encode($cdt_settings));
    $item->save();

  }

  /**
   * Writes text according to the XLIFF export settings.
   *
   * @param string $text
   *   The contents of the text.
   * @param array $key_array
   *   The source item data key.
   *
   * @return bool
   *   TRUE on success or FALSE on failure.
   */
  protected function writeData($text, array $key_array) {
    if ($this->job->getSetting('xliff_cdata')) {
      return $this->writeCdata(trim((string) $text));
    }

    if ($this->job->getSetting('xliff_processing')) {
      return $this->writeRaw($this->processForExport($text, $key_array));
    }

    return $this->text($text);
  }

  /**
   * {@inheritdoc}
   */
  public function export(JobInterface $job, $conditions = []) {

    $this->job = $job;

    $this->openMemory();
    $this->setIndent(TRUE);
    $this->setIndentString(' ');
    $this->startDocument('1.0', 'UTF-8', 'no');

    // <Transaction> Root element.
    $this->startElement('Transaction');
    $this->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    $this->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://static.cdt.europa.eu/webtranslations/schemas/Drupal-Translation-V8-2.xsd');

    $this->startElement('TransactionHeader');
    $this->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');

    $this->startElement('SenderDetails');
    $this->writeAttribute('drupalVersion', \DRUPAL::VERSION);
    $this->writeAttribute('tmgmtVersion', (string) @\Drupal::service('extension.list.module')->getExtensionInfo('tmgmt')['version']);
    $this->writeAttribute('moduleVersion', (string) @\Drupal::service('extension.list.module')->getExtensionInfo('tmgmt_cdt')['version']);
    $this->writeElement('ProducerDateTime', date('c'));
    // End tag </SenderDetails>.
    $this->endElement();
    // End tag </TransactionHeader>.
    $this->endElement();

    $this->writeElement('TransactionIdentifier', $job->id());
    $this->writeElement('TransactionCode', 'Drupal Translation Request');

    $this->startElement('TransactionData');
    $this->startElement('TranslationDetails');

    // To be sure we export one item per language (the first one orignal lang)
    $items_unique = [];
    foreach ($job->getItems() as $item) {
      if (in_array($item->get('item_id')->value, $items_unique)) {
        continue;
      }
      $this->addItem($item);
      $items_unique[] = $item->get('item_id')->value;
    }
    unset($items_unique);

    $this->writeElement('TotalCharacterLength', $job->getCounts()['characters']);
    // End tag </TransactionDetails>.
    $this->endElement();
    // End tag </TransactionData>.
    $this->endElement();

    $this->endDocument();
    return $this->outputMemory();

  }

  /**
   * {@inheritdoc}
   */
  public function import($imported_file, $is_file = TRUE) {
  }

  /**
   * {@inheritdoc}
   */
  public function importXml(int $job_id, string $xml_data, string $target_language) {
    try {
      if (!$this->importedXml) {
        throw new \Exception('Xml not valid for language ' . $target_language);
      }

      $transaction_identifier = $this->importedXml->xpath('//TransactionIdentifier');
      $transaction_identifier = (int) reset($transaction_identifier);

      if ($transaction_identifier !== $job_id) {
        throw new \Exception('Wrong TransactionIdentifier for language ' . $target_language);
      }

      $job = (Job::load((int) $transaction_identifier));

      $flat_data = $this->getImportedTargets($job, $target_language);
      // Free mem.
      $this->importedXml = NULL;
      $data = \Drupal::service('tmgmt.data')->unflatten((array) $flat_data);
      return $data;
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return [];
    }

    if ($this->getImportedXML($imported_file, $is_file) === FALSE) {
      return FALSE;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function validateImport($imported_file, $is_file = TRUE) {
    try {

      $dom = new \DOMDocument();
      $dom->validateOnParse = TRUE;

      @$dom->loadXML($imported_file);

      libxml_use_internal_errors(TRUE);
      libxml_clear_errors();

      if (!(bool) TmgmtCdtHelper::getState('disable_xsd_validation')) {

        $xml_xsd_link = trim((string) $dom->documentElement->getAttributeNS($dom->lookupNamespaceURI('xsi'), 'noNamespaceSchemaLocation'));

        $schema_source = '';

        // Check if the current xsd is the same as the stored one.
        if (TmgmtCdtHelper::getState('xsd_link') !== $xml_xsd_link) {
          $schema_source = TmgmtCdtHelper::curlGetFileContents($xml_xsd_link);
          if ($schema_source !== FALSE) {
            \Drupal::state()->set('tmgmt_cdt.xsd_source', $schema_source);
            \Drupal::state()->set('tmgmt_cdt.xsd_link', $xml_xsd_link);
          }
          else {
            throw new \Exception('Error reading content from ' . $xml_xsd_link);
          }
        }

        $schema_source = TmgmtCdtHelper::getState('xsd_source');
        @$dom->schemaValidateSource($schema_source);

        $xml_errors = libxml_get_errors();
        if (\count((array) $xml_errors) > 0) {
          \Drupal::logger('tmgmt_cdt')->debug('CdT invalid XML<pre><code>' . print_r($xml_errors, TRUE) . '</code></pre>');
          throw new \Exception('XML could not be validated by the XSD schema. Compare the XML schema with the current configuration of the CdT module');
        }
      }

      $this->importedXml = @simplexml_load_string($imported_file);

      if ($this->importedXml === FALSE) {
        throw new \Exception('Error parsing XML');
      }

      $transaction_identifier = $this->importedXml->xpath('//TransactionIdentifier');

      if ($transaction_identifier) {
        $transaction_identifier = reset($transaction_identifier);
      }
      else {
        throw new \Exception('TransactionIdentifier tag is missing');
      }

      $job = (Job::load((int) $transaction_identifier));

      if (empty($job)) {
        throw new \Exception('Cannot find matching job ID in the system');
      }
      return TRUE;
    }
    catch (\Exception $e) {
      $this->importedXml = FALSE;
      return $e->getMessage();
    }
  }

  /**
   * Job Item ids are stored in [lang][entity_type][entity_id].
   */
  protected function getMappedItemIds(JobInterface $job) {
    $job_items = $job->getItems();

    foreach ($job_items as $job_item) {
      $this->mappedItemsIDs[$job_item->get('cdt_target_language')->value][$job_item->get('item_type')->value][$job_item->get('item_id')->value] = $job_item->get('item_id')->value;
    }

  }

  /**
   * Import per target language.
   */
  protected function getImportedTargets(JobInterface $job, $target_language) {
    $this->getMappedItemIds($job);
    $importedTransUnits = [];

    $errors = 0;
    $warnings = 0;

    $fullImportedTransUnits = [];

    foreach ($this->importedXml->xpath('//Translation') as $translation) {

      if ($job->get('source_language')->value != $target_language) {
        $entity_field_list = [];
        foreach ($translation->StaticContent as $unit) {

          $entity_id = (string) $unit['instanceId'];
          $entity_type = (string) $unit['indexType'];
          $entity_field = (string) $unit['resName'];
          $res_type = (string) $unit['resType'];
          $res_max_size = (int) $unit['resMaxSize'];

          // Be sure we have a job item for this combinaison.
          if (!empty($this->mappedItemsIDs[$target_language][$entity_type][$entity_id])) {

            $job_items = $job->getItems(
                  [
                    'item_id' => $entity_id,
                    'cdt_target_language' => $target_language,
                  ]
              );
            $job_item = reset($job_items);
            try {

              if ((bool) TmgmtCdtHelper::getState('html_check')) {

                $entity_field_clean = str_replace('[', '', $entity_field);
                $entity_field_clean = str_replace(']', '', $entity_field_clean);

                $job_settings = Json::decode($job_item->get('cdt_settings')->value);
                if (!$job_settings) {
                  $job_settings = [];
                }

                if (isset($job_settings['tagsTypeCount'][$entity_field_clean])) {
                  $source_tags = $job_settings['tagsTypeCount'][$entity_field_clean];
                }
                else {
                  $source_tags = [];
                }

                $target_tags = @TmgmtCdtFormatHelper::tagsTypeCount((string) $unit);

                // Check tags quantity (but can import translation)
                if (\count((array) $source_tags) !== \count((array) $target_tags)) {
                  $warnings++;
                  $job_item->addMessage(
                        $this->t('The quantity of tags in the source and the target is different. Field: @field (@language)'),
                        [
                          '@field' => $entity_field,
                          '@language' => $target_language,
                        ], 'warning'
                    );
                }

              }

              $job_item_id = $this->mappedItemsIDs[$target_language][$entity_type][$entity_id];
              $key = $job_item_id . $entity_field;

              $converted_content = TmgmtCdtFormatHelper::convertTokenToDrupal((string) $unit);
              if (strpos($converted_content, 'drupal-token') !== FALSE) {
                throw new \Exception('The content has not been converted. Please check the translation. field: ' . $entity_field . ', language: ' . $target_language);
              }

              if ($res_type !== 'html') {

                $converted_content = html_entity_decode($converted_content);
                // @todo get this list from option
                $converted_content = \strip_tags($converted_content, '<none><front><separator><nolink><minipanel>');
              }
              if ($res_max_size > 0) {
                if (strlen($converted_content) > $res_max_size) {
                  $warnings++;
                  $job_item->addMessage(
                        $this->t('The content is too long, it will probably be truncated. Field: @field (@language)'),
                        [
                          '@field' => $entity_field,
                          '@language' => $target_language,
                        ], 'warning'
                    );
                }
              }

              $importedTransUnits[$key]['#text'] = $converted_content;
              $entity_field_list[$entity_field] = $key;

            }
            catch (\Exception $e) {
              $errors++;
              $job_item->addMessage(
                      'ERROR @message (@language)',
                      [
                        '@message' => $e->getMessage(),
                        '@language' => $target_language,
                      ], 'error'
                  );
            }
          }

        }

        $data = \Drupal::service('tmgmt.data')->filterTranslatable($job_item->getData());
        foreach ($data as $key => $value) {
          if (!in_array('][' . $key, array_keys($entity_field_list))) {
            $fullImportedTransUnits[$entity_id . '][' . $key] = ['#text' => $value['#text']];
          }
          else {
            $fullImportedTransUnits[$entity_id . '][' . $key] = $importedTransUnits[$entity_id . '][' . $key];
          }
        }
      }

    }
    unset($importedTransUnits);

    if ($warnings > 0) {
      $this->messenger()->addWarning($this->t('The import has generated @warnings warnings (but translations are imported). Please consult the message list (at the bottom of the job page)', ['@warnings' => $warnings]), FALSE);
    }

    if ($errors > 0) {
      $this->messenger()->addError($this->t('The import has generated  @errors errors (translations are not imported). Please consult the message list (at the bottom of the job page)', ['@errors' => $errors]), FALSE);
    }
    return $fullImportedTransUnits;
  }

}
