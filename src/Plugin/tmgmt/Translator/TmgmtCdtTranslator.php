<?php

namespace Drupal\tmgmt_cdt\Plugin\tmgmt\Translator;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Xss;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\tmgmt\Translator\TranslatableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt_cdt\Entity\TmgmtCdtJob;
use Drupal\tmgmt_cdt\Plugin\tmgmt_cdt\Format\TmgmtCdtXml;
use Drupal\tmgmt_cdt\TmgmtCdtApi;
use Drupal\tmgmt_cdt\TmgmtCdtFormatHelper;
use Drupal\tmgmt_cdt\TmgmtCdtHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Cdt translation plugin.
 *
 * @TranslatorPlugin(
 *   id = "tmgmt_cdt",
 *   label = @Translation("Translation Centre (CdT)"),
 *   description = @Translation("Translation Centre For the Bodies of the EU https://cdt.europa.eu - Translator for TMGMT."),
 *   logo = "icons/cdt.svg",
 *   ui = "Drupal\tmgmt_cdt\TmgmtCdtTranslatorUi",
 * )
 */
class TmgmtCdtTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
    $configuration,
    $pluginId,
    $pluginDefinition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hasCheckoutSettings(JobInterface $job) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ((bool) TmgmtCdtHelper::getState('ready')) {
      return AvailableResult::yes();
    }
    return AvailableResult::no(
          $this->t(
              '@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
                '@translator' => $translator->label(),
                ':configured' => $translator->toUrl()->toString(),
              ]
          )
      );

  }

  /**
   * {@inheritdoc}
   */
  public function checkTranslatable(TranslatorInterface $translator, JobInterface $job) {
    if ($job->isRejected()) {
      return TranslatableResult::no($this->t('Cannot process a rejected job.'));
    }

    $errors = TmgmtCdtHelper::checkJob($job);
    if (!empty($errors)) {
      foreach ($errors as $message) {
        $this->messenger()->addError($message);
      }
      return TranslatableResult::no($this->t('Cannot process this job.'));
    }

    return TranslatableResult::yes();
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {

    try {

      $file_name = 'cdt_job_' . $job->id() . '_request.xml';
      $file_format = new TmgmtCdtXml($this->configuration, $this->pluginId, $this->pluginDefinition);
      $file_content = $file_format->export($job);

      $directory = 'public://tmgmt_file';
      \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $file = \Drupal::service('file.repository')->writeData($file_content, "public://tmgmt_file/$file_name", FileSystemInterface::EXISTS_REPLACE);
      if (!$file) {
        throw new \Exception('Cannot create Xml file');
      }
      \Drupal::service('file.usage')->add($file, 'tmgmt_file', 'tmgmt_job', $job->id());
      $job->addMessage('XML file created, can be downloaded <a href="@link" download>here</a>.', ['@link' => \Drupal::service('file_url_generator')->generateAbsoluteString("public://tmgmt_file/$file_name")]);

      $request_params = $this->generateRequest($job, $file_content);
      $file_content = NULL;

      $job->set('cdt_request_params', \json_encode($request_params));
      $job->set('target_language', 'multiple');
      $job->set('cdt_request_identifier', TmgmtCdtHelper::DEFAULT_REQUEST_IDENTIFIER);
      $job->save();

      $api = TmgmtCdtApi::getInstance();
      $validate_request = $api->validateRequest($request_params);

      if (!$validate_request) {
        throw new \Exception('Cannot validate request');
      }

      $job->jobItemGenerateTargetLanguages();

      $api->resetInstance();
      $api->setParamsBody($request_params);
      $api->post('requests');

      if ($api->response->status !== 'success') {
        throw new \Exception(implode(', ', $api->response->errors));
      }

      $job->submitted(
            'Translation job <strong>@job_id</strong> successfully submitted to @name. (Temporary id: @correlation_id). Please go to the <a href="@job_url">job page</a> to retrieve the final CDT reference (Request number)',
            [
              '@job_id' => $job->id(),
              '@job_url' => $job->toUrl()->toString(),
              '@name' => TmgmtCdtHelper::NAME,
              '@correlation_id' => trim((string) $api->response->content),
            ]
        );
      $job->set('cdt_correlation_id', substr(trim((string) $api->response->content, '"'), 0, 16));
      $job->set('cdt_request_identifier', TmgmtCdtHelper::DEFAULT_REQUEST_IDENTIFIER);

      $job->save();

      // If all ok, we save form values as defaut for the current user.
      $user_cdt_default = [
        'priority' => $job->getSetting('cdt')['priority'],
        'confidentiality' => $job->getSetting('cdt')['confidentiality'],
        'department' => $job->getSetting('cdt')['department'],
        'contacts' => $job->getSetting('cdt')['contacts'],
        'deliver_to' => $job->getSetting('cdt')['deliver_to'],
        'options' => $job->getSetting('cdt')['options'],
        'phone' => $job->getSetting('cdt')['phone'],
      ];

      /* $current_user = \Drupal::entityTypeManager()->getStorage('user')->load(\Drupal::currentUser()->id());
      $current_user->set("cdt_default", Json::encode($user_cdt_default));
      $current_user->save();*/

      \Drupal::service('user.data')->set('tmgmt_cdt', \Drupal::currentUser()->id(), 'cdt_default', Json::encode($user_cdt_default));

    }
    catch (\Exception $e) {

      $message = $this->t('Job has been rejected with following error: @error', ['@error' => $e->getMessage()]);
      $this->messenger()->addError($message);
      $job->rejected($message);
      $job->addMessage($message, 'error');

    }
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation(JobInterface $job) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $list = [];
    $remote_languages = @TmgmtCdtHelper::getState('business_reference_data')->languages;
    if ($remote_languages) {
      foreach ($remote_languages as $key) {
        $list[$key] = $key;
      }
    }
    return $list;

  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTargetLanguages(TranslatorInterface $translator, $source_language) {
    $remote_languages = $this->getSupportedRemoteLanguages($translator);

    // There are no language pairs, any supported language can be translated
    // into the others. If the source language is part of the languages,
    // then return them all, just remove the source language.
    if (array_key_exists($source_language, $remote_languages)) {
      unset($remote_languages[$source_language]);
      return $remote_languages;
    }

    return [];
  }

  /**
   * Generate parameters for the request API.
   */
  private function generateRequest(TmgmtCdtJob $job, $xml_data) {

    $job_settings = $job->getSetting('cdt');

    $params = new \stdClass();
    $params->departmentCode = $job_settings['department'];

    $params->contactUserNames = [];
    foreach ($job_settings['contacts'] as $contact) {
      array_push($params->contactUserNames, $contact);
    }

    $params->deliveryContactUsernames = [];

    foreach ($job_settings['deliver_to'] as $contact) {
      array_push($params->deliveryContactUsernames, $contact);
    }

    $phone = preg_replace('/\D/', '', $job_settings['phone']);
    $phone = mb_substr($phone, 0, 15);
    if (empty($phone)) {
      $phone = '0';
    }
    $params->phoneNumber = $phone;

    $params->title = $job->label();
    $params->clientReference = $job->id();

    $params->purposeCode = 'WS';
    $params->priorityCode = $job_settings['priority'];
    $params->deliveryModeCode = 'No';
    $params->translationsWebUploadedCode = 'No';

    $comments = '';
    $comments .= 'Url(s): <strong>' . TmgmtCdtHelper::API_COMMENT_URLS[$job_settings['options']] . '</strong><br />';
    if (\count((array) $job_settings['related_jobs']) > 0) {
      $comments .= 'Related job(s): <strong>' . implode(', ', $job_settings['related_jobs']) . '</strong>' . '<br />';
    }
    if (mb_strlen(trim((string) $job_settings['comments'])) > 0) {
      $comments .= 'Client comment: <i>' . Xss::filter($job_settings['comments']) . '</i>';
    }

    $params->comments = $comments;
    unset($comments);

    $urls = [];

    $job_items = $job->getItems();
    $items_unique = [];
    $translator = $job->getTranslator();

    foreach ($job_items as $job_item) {
      if (in_array($job_item->get('item_id')->value, $items_unique)) {
        continue;
      }

      $item_meta = TmgmtCdtFormatHelper::itemSourceReference($job_item);

      $ref = new \stdClass();
      $ref->url = $item_meta['link'];
      $ref->shortName = $item_meta['label'];
      $ref->referenceLanguages = [$translator->mapToRemoteLanguage($job_item->get('cdt_source_language')->value)];

      $urls[] = $ref;
      $items_unique[] = $job_item->get('item_id')->value;
    }

    unset($items_unique);

    $params->referenceSet = new \stdClass();
    $params->referenceSet->urls = $urls;
    unset($urls);

    $params->sourceDocuments = [];
    $params->sourceDocuments[0] = new \stdClass();
    $params->sourceDocuments[0]->file = new \stdClass();
    $params->sourceDocuments[0]->file->fileName = 'translation_job_' . $job->id() . '_request.xml';
    $params->sourceDocuments[0]->file->base64Data = base64_encode($xml_data);

    $params->sourceDocuments[0]->sourceLanguages = [
      $translator->mapToRemoteLanguage($job->get('source_language')->value),
    ];
    $params->sourceDocuments[0]->outputDocumentFormatCode = 'XM';
    $params->sourceDocuments[0]->translationJobs = [];

    $counts = $job->getCounts();

    foreach ($job_settings['target_languages'] as $id => $lang) {
      $_job = new \stdClass();
      $_job->volume = $counts['pages_language'];
      $_job->sourceLanguage = $translator->mapToRemoteLanguage($job->get('source_language')->value);
      $_job->targetLanguage = $translator->mapToRemoteLanguage($lang);
      $params->sourceDocuments[0]->translationJobs[] = $_job;
    }

    $params->sourceDocuments[0]->confidentialityCode = $job_settings['confidentiality'];

    $params->sendOptions = 'Send';

    switch ($job_settings['request_type']) {
      case 'LIGHT_POST_EDITING':
        $params->service = 'LightPostEditing';
        break;

      default:
        $params->service = 'Translation';
        break;
    }

    $params->isQuotationOnly = $job_settings['quotation'] == TRUE;

    $params->callbacks = [];

    return $params;
  }

  /**
   * Check params bettwen request and response.
   */
  public function validateRequestResponse($job, $response) {
    if (\count((array) $response) !== \count((array) $job->getSetting('cdt')['target_languages'])) {
      throw new \Exception('Request and response do not have the same number of target languages');
    }
  }

}
