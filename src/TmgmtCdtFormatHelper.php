<?php

namespace Drupal\tmgmt_cdt;

use Drupal\tmgmt\JobItemInterface;

/**
 * Helper.
 */
class TmgmtCdtFormatHelper {

  /**
   * ConvertTokenToDrupal.
   */
  public static function convertTokenToDrupal($html) {

    $item = self::getToken($html);

    $count = 0;
    while ($item !== FALSE) {
      $count++;
      $token = self::extractTokenData($item);
      $new_tag = self::restoreToken($token);
      $token_html = self::getTagByEncoded($html, $token['encoded']);
      $html = str_replace($token_html, $new_tag, $html);
      $item = self::getToken($html);
      // Anti infinite loop ...
      if ($count > 3000) {
        $item = FALSE;
      }
    }
    return $html;

  }

  /**
   * To drupal.
   */
  public static function convertDrupalToToken($html) {

    // Correct order script>drupal>img>a.
    if ((bool) TmgmtCdtHelper::getState('use_token_html_script')) {
      $list = self::getTagsHtmlScript($html);
      foreach ($list as $key => $item) {
        $token_html = self::encodeToken($item);
        $html = str_replace($item['original_html'], $token_html, $html);
      }
      unset($key);
    }

    if ((bool) TmgmtCdtHelper::getState('use_token_drupal')) {
      $list = self::getTagsDrupal($html);
      foreach ($list as $key => $item) {
        $token_html = self::encodeToken($item);
        $html = str_replace($item['original_html'], $token_html, $html);
      }
    }
    if ((bool) TmgmtCdtHelper::getState('use_token_html_img')) {
      $list = self::getTagsHtmlImg($html);
      foreach ($list as $key => $item) {
        $token_html = self::encodeToken($item);
        $html = str_replace($item['original_html'], $token_html, $html);
      }
    }

    if ((bool) TmgmtCdtHelper::getState('use_token_html_a')) {
      $list = self::getTagsHtmlA($html);
      foreach ($list as $key => $item) {
        $token_html = self::encodeToken($item);
        $html = str_replace($item['original_html'], $token_html, $html);
      }
    }
    return $html;
  }

  /**
   * Check if html or not.
   */
  public static function isHtml($text) {
    $raw_text = \strip_tags($text);
    if ($raw_text !== $text) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Find Tokens.
   */
  public static function getToken($html) {
    preg_match_all('/<drupal-token ((?!<drupal-token ).)*?<\/drupal-token>/ims', $html, $finds, PREG_SET_ORDER, 0);
    if (\count((array) $finds) > 0) {
      preg_match_all('/<drupal-token ((?!<drupal-token ).)*?<\/drupal-token>/ims', mb_substr($finds[0][0], 13, -13), $sub_finds, PREG_SET_ORDER, 0);
      if (\count((array) $sub_finds) > 0) {
        return self::getToken($sub_finds[0][0]);
      }
      return $finds[0][0];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Extract.
   */
  public static function extractTokenData($token_html) {
    $token = [
      'token_html' => $token_html,
      'encoded' => '',
      'original_html' => '',
      'type' => '',
      'items' => [],
    ];
    preg_match_all('/encoded=[\"\'](.*?)[\"\']/im', $token_html, $finds, PREG_SET_ORDER, 0);
    if (isset($finds[0][1])) {
      $token['encoded'] = $finds[0][1];
      $token['original_html'] = base64_decode($finds[0][1]);
    }
    preg_match_all('/type=[\"\'](.*?)[\"\']/im', $token_html, $finds, PREG_SET_ORDER, 0);
    if (isset($finds[0][1])) {
      $token['type'] = $finds[0][1];
    }
    preg_match_all('/<drupal-token-text[\s]+([^>]+)>((?:.(?!\<\/drupal-token-text\>))*.)<\/drupal-token-text>/ims', $token_html, $matchs, PREG_SET_ORDER, 0);
    foreach ($matchs as $id => $item) {
      $token['items'][explode('"', $item[1])[1]] = $item[2];
    }
    unset($id);
    return $token;
  }

  /**
   * RestoreDrupalMediaToken.
   */
  public static function restoreDrupalMediaToken($token) {
    $content = mb_substr($token['original_html'], 2);
    $content = mb_substr($content, 0, -2);
    try {

      // Need native json format.
      $content = \json_decode($content, TRUE);
      foreach ($token['items'] as $path => $value) {
        self::arraySetValue($content, $path, $value);
      }
      // Need native json format.
      $content = '[[' . \json_encode($content, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) . ']]';

    }
    catch (\Exception $e) {
    }
    return $content;
  }

  /**
   * RestoreDrupalSimpleToken.
   */
  public static function restoreDrupalSimpleToken($token) {
    return $token['original_html'];
  }

  /**
   * RestoreDrupalSimpleToken.
   */
  public static function restoreDrupalYamlToken($token) {
    return $token['original_html'];
  }

  /**
   * RestoreHtmlScript.
   */
  public static function restoreHtmlScript($token) {
    return $token['original_html'];
  }

  /**
   * RestoreHtmlA.
   */
  public static function restoreHtmlA($token) {
    $doc = new \DomDocument('1.0', 'UTF-8');
    libxml_use_internal_errors(TRUE);
    $doc->loadHTML($token['original_html']);
    $tag = $doc->getElementsByTagName('a')->item(0);

    foreach ($token['items'] as $path => $value) {
      if ($path === 'label') {
        $tag->nodeValue = '';
        $fragment = $doc->createDocumentFragment();
        $fragment->appendXML(html_entity_decode($value));
        $tag->appendChild($fragment);
      }
      else {
        $tag->setAttribute($path, html_entity_decode($value));
      }
    }
    return $tag->ownerDocument->saveXML($tag);
  }

  /**
   * RestoreHtmlImg.
   */
  public static function restoreHtmlImg($token) {
    $doc = new \DomDocument('1.0', 'UTF-8');
    libxml_use_internal_errors(TRUE);
    $doc->loadHTML($token['original_html']);
    $tag = $doc->getElementsByTagName('img')->item(0);
    foreach ($token['items'] as $path => $value) {
      $tag->setAttribute($path, $value);
    }
    return $tag->ownerDocument->saveXML($tag);
  }

  /**
   * RestoreToken.
   */
  public static function restoreToken($token) {
    $html_tag = '';
    switch ($token['type']) {
      case TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_MEDIA:
        $html_tag = self::restoreDrupalMediaToken($token);
        break;

      case TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_SIMPLE:
        $html_tag = self::restoreDrupalSimpleToken($token);
        break;

      case TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_YAML:
        $html_tag = self::restoreDrupalYamlToken($token);
        break;

      case TmgmtCdtHelper::TOKEN_TYPE_HTML_SCRIPT:
        $html_tag = self::restoreHtmlScript($token);
        break;

      case TmgmtCdtHelper::TOKEN_TYPE_HTML_A:
        $html_tag = self::restoreHtmlA($token);
        break;

      case TmgmtCdtHelper::TOKEN_TYPE_HTML_IMG:
        $html_tag = self::restoreHtmlImg($token);
        break;
    }
    return $html_tag;
  }

  /**
   * GetTagByEncoded.
   */
  public static function getTagByEncoded($html, $encoded) {
    $original_html = FALSE;
    preg_match_all('/<drupal-token ((?!<drupal-token ).)*?<\/drupal-token>/ims', $html, $matchs, PREG_SET_ORDER, 0);
    foreach ($matchs as $id => $item) {
      preg_match_all('/encoded=[\"\'](.*?)[\"\']/im', $item[0], $finds, PREG_SET_ORDER, 0);
      if ($finds[0][1] === $encoded) {
        $original_html = $item[0];
      }
    }
    unset($id);
    return $original_html;
  }

  /**
   * EncodeToken.
   */
  public static function encodeToken($data) {
    try {
      $token_list = [TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_MEDIA, TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_SIMPLE,
        TmgmtCdtHelper::TOKEN_TYPE_HTML_A, TmgmtCdtHelper::TOKEN_TYPE_HTML_IMG, TmgmtCdtHelper::TOKEN_TYPE_HTML_SCRIPT,
        TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_YAML,
      ];
      if (!in_array($data['type'], $token_list)) {
        throw new \Exception('Unknown tag: ' . $data['type']);
      }
      $html = '<drupal-token type="' . $data['type'] . '" encoded="' . $data['encoded'] . '">';
      foreach ($data['texts'] as $key => $text) {
        $html .= '<drupal-token-text path="' . $key . '">' . $text . '</drupal-token-text>';
      }
      $html .= '</drupal-token>';
      return $html;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * From https://stackoverflow.com/questions/9628176/using-a-string-path-to-set-nested-array-data/44189105#44189105.
   *
   * @todo check if we can replace by \Drupal\Component\Utility\NestedArray ?
   */
  public static function arraySetValue(&$data, $parents, $value, $glue = '.') {
    if (!\is_array($parents)) {
      $parents = explode($glue, (string) $parents);
    }
    $ref = &$data;
    foreach ($parents as $parent) {
      if (isset($ref) && !\is_array($ref)) {
        $ref = [];
      }
      $ref = &$ref[$parent];
    }
    $ref = $value;
  }

  /**
   * GetTagsDrupal.
   */
  public static function getTagsDrupal($html) {
    try {

      $list = [];
      // Media token [[...]].
      preg_match_all("/\[\[.*?\]\]/s", $html, $matches, PREG_SET_ORDER, 0);
      foreach ($matches as $id => $value) {
        $original_html = $value[0];
        $encoded = base64_encode($original_html);

        $result = [
          'original_html' => $original_html,
          'encoded' => $encoded,
          'type' => TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_MEDIA,
          'title' => 'Drupal media Token',
          'texts' => [],
        ];
        $content = $result['original_html'];
        // Remove [[.
        $content = mb_substr($content, 2);
        // Remove ]].
        $content = mb_substr($content, 0, -2);

        // Need native json format.
        $contentObject = \json_decode($content);

        if (isset($contentObject->attributes->alt)) {
          $result['texts']['attributes.alt'] = $contentObject->attributes->alt;
        }
        if (isset($contentObject->attributes->title)) {
          $result['texts']['attributes.title'] = $contentObject->attributes->title;
        }
        $list[] = $result;
        $html = str_replace($original_html, '', $html);
      }
      unset($id);

      // Simple token [...].
      preg_match_all("/\[.*?\]/s", $html, $matches, PREG_SET_ORDER, 0);
      foreach ($matches as $id => $value) {
        $original_html = $value[0];
        $encoded = base64_encode($original_html);
        $result = [
          'original_html' => $original_html,
          'encoded' => $encoded,
          'type' => TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_SIMPLE,
          'title' => 'Drupal Token',
          'texts' => [],
        ];
        $list[] = $result;
        $html = str_replace($original_html, '', $html);
      }

      // Simple token {{...}}.
      /*
      preg_match_all("/{{.*?}}/s", $html, $matches, PREG_SET_ORDER, 0);
      foreach ($matches as $id => $value) {
      $original_html = $value[0];
      $encoded = base64_encode($original_html);
      $result = [
      'original_html' => $original_html,
      'encoded' => $encoded,
      'type' => TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_SIMPLE,
      'title' => 'Drupal Token',
      'texts' => [],
      ];
      $list[] = $result;
      $html = str_replace($original_html, '', $html);
      }
       */

      // Special tags.
      preg_match_all("/(<none>|<front>|<separator>|<nolink>|<minipanel>|_default|_none)/s", $html, $matches, PREG_SET_ORDER, 0);
      // @todo place this list as an editable option
      foreach ($matches as $id => $value) {
        $original_html = $value[0];
        $encoded = base64_encode($original_html);
        $result = [
          'original_html' => $original_html,
          'encoded' => $encoded,
          'type' => TmgmtCdtHelper::TOKEN_TYPE_DRUPAL_TOKEN_SIMPLE,
          'title' => 'Drupal special Tag',
          'texts' => [],
        ];
        $list[] = $result;
        $html = str_replace($original_html, '', $html);
      }

      return $list;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * GetTagsHtmlA.
   */
  public static function getTagsHtmlA($html) {
    try {
      $list = [];
      preg_match_all('/<a[\s]+([^>]+)>((?:.(?!\<\/a\>))*.)<\/a>/ims', $html, $matchs, PREG_SET_ORDER, 0);
      foreach ($matchs as $id => $item) {
        $original_html = $item[0];
        $encoded = base64_encode($original_html);
        $result = [
          'original_html' => $original_html,
          'encoded' => $encoded,
          'type' => TmgmtCdtHelper::TOKEN_TYPE_HTML_A,
          'title' => 'HTML link',
          'texts' => [
            'label' => trim((string) $item[2]),
          ],
        ];
        preg_match_all('/href=[\"\'](.*?)[\"\']/im', $result['original_html'], $finds, PREG_SET_ORDER, 0);
        if (isset($finds[0][1])) {
          $result['texts']['href'] = $finds[0][1];
        }
        preg_match_all('/hreflang=[\"\'](.*?)[\"\']/im', $result['original_html'], $finds, PREG_SET_ORDER, 0);
        if (isset($finds[0][1])) {
          $result['texts']['hreflang'] = $finds[0][1];
        }
        preg_match_all('/lang=[\"\'](.*?)[\"\']/im', $result['original_html'], $finds, PREG_SET_ORDER, 0);
        if (isset($finds[0][1])) {
          $result['texts']['lang'] = $finds[0][1];
        }
        $list[] = $result;
      }
      unset($id);
      return $list;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * GetTagsHtmlImg.
   */
  public static function getTagsHtmlImg($html) {
    try {
      $list = [];
      preg_match_all('/<img([\w\W]+?)>/ims', $html, $matchs, PREG_SET_ORDER, 0);
      foreach ($matchs as $id => $item) {
        $original_html = $item[0];
        $encoded = base64_encode($original_html);
        $result = [
          'original_html' => $original_html,
          'encoded' => $encoded,
          'type' => TmgmtCdtHelper::TOKEN_TYPE_HTML_IMG,
          'title' => 'HTML link',
          'texts' => [],
        ];
        preg_match_all('/alt=[\"\'](.*?)[\"\']/im', $result['original_html'], $finds, PREG_SET_ORDER, 0);
        if (isset($finds[0][1])) {
          $result['texts']['alt'] = $finds[0][1];
        }
        preg_match_all('/title=[\"\'](.*?)[\"\']/im', $result['original_html'], $finds, PREG_SET_ORDER, 0);
        if (isset($finds[0][1])) {
          $result['texts']['title'] = $finds[0][1];
        }
        preg_match_all('/lang=[\"\'](.*?)[\"\']/im', $result['original_html'], $finds, PREG_SET_ORDER, 0);
        if (isset($finds[0][1])) {
          $result['texts']['lang'] = $finds[0][1];
        }
        $list[] = $result;
      }
      unset($id);
      return $list;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * GetTagsHtmlScript.
   */
  public static function getTagsHtmlScript($html) {
    try {
      $list = [];

      $doc = new \DomDocument('1.0', 'UTF-8');
      libxml_use_internal_errors(TRUE);

      $doc->loadHTML($html);
      $tags = $doc->getElementsByTagName("script");
      foreach ($tags as $tag) {
        $original_html = $doc->saveHTML($tag);
        $encoded = base64_encode($original_html);
        $result = [
          'original_html' => $original_html,
          'encoded' => $encoded,
          'type' => TmgmtCdtHelper::TOKEN_TYPE_HTML_SCRIPT,
          'title' => 'HTML script',
          'texts' => [],
        ];
        $list[] = $result;
      }
      return $list;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * Get html tags count.
   */
  public static function tagsCount($html) {
    preg_match_all('/<((?=!\-\-)!\-\-[\s\S]*\-\-|((?=\?)\?[\s\S]*\?|((?=\/)\/[^.\-\d][^\/\]\'"[!#$%&()*+,;<=>?@^`{|}~ ]*|[^.\-\d][^\/\]\'"[!#$%&()*+,;<=>?@^`{|}~ ]*(?:\s[^.\-\d][^\/\]\'"[!#$%&()*+,;<=>?@^`{|}~ ]*(?:=(?:"[^"]*"|\'[^\']*\'|[^\'"<\s]*))?)*)\s?\/?))>/mi', $html, $matchs);
    return \count((array) $matchs[0]);
  }

  /**
   * TagsTypeCount.
   */
  public static function tagsTypeCount($html) {
    preg_match_all('/<((?=!\-\-)!\-\-[\s\S]*\-\-|((?=\?)\?[\s\S]*\?|((?=\/)\/[^.\-\d][^\/\]\'"[!#$%&()*+,;<=>?@^`{|}~ ]*|[^.\-\d][^\/\]\'"[!#$%&()*+,;<=>?@^`{|}~ ]*(?:\s[^.\-\d][^\/\]\'"[!#$%&()*+,;<=>?@^`{|}~ ]*(?:=(?:"[^"]*"|\'[^\']*\'|[^\'"<\s]*))?)*)\s?\/?))>/mi', $html, $matchs);
    $list = [];
    foreach (array_values($matchs[1]) as $val) {
      $exp = explode(' ', mb_strtolower($val));
      if (!isset($list[$exp[0]])) {
        $list[$exp[0]] = 0;
      }
      $list[$exp[0]]++;
    }
    return $list;
  }

  /**
   * ItemSourceReference.
   */
  public static function itemSourceReference(JobItemInterface $job_item) {

    $website_url_home = \Drupal::request()->getSchemeAndHttpHost();
    $website_name = \Drupal::config('system.site')->get('name');

    $item = [
      'link' => '',
      'label' => '',
    ];

    switch ($job_item->get('item_type')->value) {

      case 'node':
      case 'taxonomy_term':
        $entity = \Drupal::entityTypeManager()->getStorage($job_item->get('item_type')->value)->load($job_item->get('item_id')->value);
        $item['link'] = $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
        $item['label'] = $entity->label();
        break;

      default:
        $item['link'] = $website_url_home;
        $item['label'] = $website_name;

    }

    return $item;

  }

  /**
   * Character count with CdT rules.
   */
  public static function characterCount($text) {
    $text = \strip_tags($text);
    $text = trim((string) str_replace(' ', '', $text));
    $text = preg_replace('/\x{00A0}|&nbsp;|\s/', '', $text);

    return mb_strlen($text);
  }

  /**
   * Extract urls (href).
   */
  public static function extractHrefs($html) {

    $hrefs = [];
    preg_match_all('/<a[\s]+([^>]+)>((?:.(?!\<\/a\>))*.)<\/a>/im', $html, $matchs, PREG_SET_ORDER, 0);

    foreach ($matchs as $id => $item) {
      preg_match_all('/href=[\"\'](.*?)[\"\']/im', $item[0], $href, PREG_SET_ORDER, 0);
      if (empty($href)) {
        continue;
      }
      $hrefs[] = [
            // Full tag.
        'a' => $item[0],
        'label' => $item[2],
        'href' => $href[0][1] ?? '',
      ];
    }
    unset($id);
    return $hrefs;
  }

  /**
   * Convert webform options.
   */
  public static function convertWebformOptions($text) {
    $new_list = '';
    $converted = FALSE;
    // Split by line.
    $list = explode("\n", $text);

    foreach ($list as $item) {
      // Split by key/value.
      $data = explode(': ', $item);

      // Only if we have a key/value format.
      if (isset($data[1])) {
        $_key = $data[0];
        $_value = $data[1];

        if ($_value[0] === "'" && substr($_value, -1) === "'") {
          $_value = substr($_value, 1, -1);
        }

        $new_list .= '<drupal-token type="drupal_token_yaml" encoded="' . \base64_encode($_key . ': ') . '"></drupal-token>' . $_value . "\n";
        $converted = TRUE;
      }
    }

    if ($converted) {
      // Trim to remove last new line character.
      return trim((string) $new_list);
    }
    else {
      return $text;
    }

  }

}
