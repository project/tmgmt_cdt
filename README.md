INTRODUCTION
------------

This module is a plugin for the Translation Management Tool.
It connects a Drupal site with Cdt API for the purpose of
streamlining translation processes.  
Website: https://www.cdt.europa.eu/

REQUIREMENTS
------------

This module requires the following modules:

 * TMGMT (https://www.drupal.org/project/tmgmt)
 
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure your Cdt plugin in Administration » Translation » Providers.
 
TROUBLESHOOTING
---------------

 * In case of defect, please log an issue on Drupal version control.
 
 * For any assistance, please contact our support helpdesk:
   https://www.cdt.europa.eu/
